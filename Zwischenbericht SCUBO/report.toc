\contentsline {chapter}{Abstract}{v}{chapter*.2}
\contentsline {chapter}{Preface}{vii}{chapter*.3}
\contentsline {chapter}{Symbols}{ix}{chapter*.4}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}{\normalfont \textsc {Scubo}} - our Robot}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}What is a Focus Project?}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}{\normalfont \textsc {Sepios}} and {\normalfont \textsc {Nanins}}}{3}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}Goals and Vision}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Key Features}{4}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Omnidirectionality}{4}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Modularity}{4}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}3D Modelling}{5}{subsection.1.4.3}
\contentsline {subsection}{\numberline {1.4.4}Telepresence}{5}{subsection.1.4.4}
\contentsline {section}{\numberline {1.5}Main Challenges}{5}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Localisation}{5}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Collision Avoidance}{5}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Sealing}{5}{subsection.1.5.3}
\contentsline {subsection}{\numberline {1.5.4}Maintenance}{6}{subsection.1.5.4}
\contentsline {section}{\numberline {1.6}Conventions}{6}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Coordinates}{6}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}Parts of the robot}{6}{subsection.1.6.2}
\contentsline {subsubsection}{Main box}{6}{section*.8}
\contentsline {subsubsection}{Hatches}{7}{section*.9}
\contentsline {subsubsection}{Thruster}{7}{section*.10}
\contentsline {subsubsection}{VI-Sensor}{7}{section*.11}
\contentsline {subsubsection}{Camera}{7}{section*.12}
\contentsline {subsubsection}{Modular ports}{7}{section*.13}
\contentsline {subsubsection}{Outer shell}{7}{section*.14}
\contentsline {subsection}{\numberline {1.6.3}Special terms in the report }{8}{subsection.1.6.3}
\contentsline {subsubsection}{Modularity}{8}{section*.15}
\contentsline {subsubsection}{Omnidirectionality}{8}{section*.16}
\contentsline {section}{\numberline {1.7}The Team}{8}{section.1.7}
\contentsline {section}{\numberline {1.8}Project Organization}{8}{section.1.8}
\contentsline {subsection}{\numberline {1.8.1}Team management}{8}{subsection.1.8.1}
\contentsline {subsection}{\numberline {1.8.2}Timetable and Milestones}{10}{subsection.1.8.2}
\contentsline {subsection}{\numberline {1.8.3}Management supporting software}{11}{subsection.1.8.3}
\contentsline {section}{\numberline {1.9}Costs and Sponsoring}{11}{section.1.9}
\contentsline {chapter}{\numberline {2}Design Process}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Guidelines by \textsc {DRZ}}{14}{section.2.1}
\contentsline {section}{\numberline {2.2}Existing Underwater Robots}{14}{section.2.2}
\contentsline {section}{\numberline {2.3}Requirements}{16}{section.2.3}
\contentsline {section}{\numberline {2.4}Concept}{17}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Shape}{17}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Cameras and Light}{17}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Localisation and Collision Avoidance}{18}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Propulsion}{19}{subsection.2.4.4}
\contentsline {subsubsection}{Evaluation of arrangement}{19}{section*.17}
\contentsline {subsection}{\numberline {2.4.5}Duct in Box}{20}{subsection.2.4.5}
\contentsline {subsection}{\numberline {2.4.6}Accessibility}{20}{subsection.2.4.6}
\contentsline {subsection}{\numberline {2.4.7}Final concept}{21}{subsection.2.4.7}
\contentsline {section}{\numberline {2.5}Subsystems}{21}{section.2.5}
\contentsline {section}{\numberline {2.6}Risk Analysis}{22}{section.2.6}
\contentsline {chapter}{\numberline {3}Mechanical Design}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Overview}{26}{section.3.1}
\contentsline {section}{\numberline {3.2}Central Unit}{26}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Dimensions}{26}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Material}{27}{subsection.3.2.2}
\contentsline {subsubsection}{Corrosion}{28}{section*.18}
\contentsline {subsection}{\numberline {3.2.3}Hatches}{28}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Supports}{28}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Exterior Parts}{28}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Modular Ports}{28}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Thruster Arms}{29}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}VI-Sensor Housing}{30}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Front and Rear Bumper}{30}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Lamp Support}{31}{subsection.3.3.5}
\contentsline {section}{\numberline {3.4}Weight and Balance}{31}{section.3.4}
\contentsline {section}{\numberline {3.5}Sealing}{32}{section.3.5}
\contentsline {subsubsection}{Cable Glands}{32}{section*.19}
\contentsline {subsubsection}{O-Rings}{32}{section*.20}
\contentsline {subsubsection}{Gaskets}{32}{section*.21}
\contentsline {section}{\numberline {3.6}FEM Check-Up}{32}{section.3.6}
\contentsline {chapter}{\numberline {4}Electronics}{35}{chapter.4}
\contentsline {section}{\numberline {4.1}Cameras}{36}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Challenges in Photography}{36}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Selection of Cameras and Lenses}{36}{subsection.4.1.2}
\contentsline {subsubsection}{Focal lengths and aperture of the cameras}{37}{section*.22}
\contentsline {subsubsection}{Zoom effect}{38}{section*.23}
\contentsline {section}{\numberline {4.2}Lights}{38}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}General need for Light Sources}{38}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Amount of Luminous Flux}{39}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Postion of the Lamps}{39}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Opening Angle of the Light Cone}{39}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Outlook}{39}{subsection.4.2.5}
\contentsline {section}{\numberline {4.3}Sensors}{40}{section.4.3}
\contentsline {section}{\numberline {4.4}Central Computing Unit}{41}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Vision Unit - Intel NUC}{41}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Control Unit - UDOO QUAD}{42}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Battery}{42}{subsection.4.4.3}
\contentsline {section}{\numberline {4.5}VI Sensor}{43}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Localisation}{44}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Collision Avoidance}{44}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}Laser}{44}{section.4.6}
\contentsline {chapter}{\numberline {5}Software}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}Operating System}{46}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Robot Operating System (ROS)}{46}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Situation}{46}{subsection.5.1.2}
\contentsline {subsubsection}{Communication Structure}{46}{section*.24}
\contentsline {subsubsection}{Communication between Machines}{47}{section*.25}
\contentsline {subsection}{\numberline {5.1.3}Software Packages}{47}{subsection.5.1.3}
\contentsline {subsubsection}{Version Control}{47}{section*.26}
\contentsline {section}{\numberline {5.2}Modelling}{48}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Kinematics}{48}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Dynamics}{49}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Simulink Model}{50}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Computer Vision}{50}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Camera Calibration}{50}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}3D Modelling}{51}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Virtual Reality}{52}{subsection.5.3.3}
\contentsline {subsubsection}{Virtual Reality Headset}{52}{section*.27}
\contentsline {chapter}{\numberline {6}Summary}{55}{chapter.6}
\contentsline {section}{\numberline {6.1}Conclusion}{55}{section.6.1}
\contentsline {section}{\numberline {6.2}Outlook}{55}{section.6.2}
\contentsline {chapter}{Bibliography}{61}{chapter*.30}
\contentsline {chapter}{\numberline {A}Fundamentals}{63}{appendix.A}
\contentsline {section}{\numberline {A.1}Modelling Definitions}{63}{section.A.1}
\contentsline {section}{\numberline {A.2}E-Mail transcript with Markus Inglin}{65}{section.A.2}
\contentsline {section}{\numberline {A.3}Camera Evaluation}{66}{section.A.3}
\contentsline {section}{\numberline {A.4}Volume Calculations}{67}{section.A.4}
\contentsline {section}{\numberline {A.5}Full Analysis Central Unit}{68}{section.A.5}
\contentsline {section}{\numberline {A.6}Weight Calculations}{71}{section.A.6}
\contentsline {section}{\numberline {A.7}List of Requirements}{72}{section.A.7}
\contentsline {section}{\numberline {A.8}FMEA}{73}{section.A.8}
\contentsline {section}{\numberline {A.9}FMEA Description}{74}{section.A.9}
\contentsline {section}{\numberline {A.10}List of Clamps}{75}{section.A.10}
\contentsline {section}{\numberline {A.11}Weight and Balance}{76}{section.A.11}
\contentsline {section}{\numberline {A.12}Gantt-Diagram}{77}{section.A.12}
\contentsline {section}{\numberline {A.13}Functional Overview}{78}{section.A.13}
\contentsline {section}{\numberline {A.14}Security Concept}{79}{section.A.14}
\contentsline {section}{\numberline {A.15}Benefit Analysis}{82}{section.A.15}
\contentsline {section}{\numberline {A.16}Estimation Battery Capacity}{83}{section.A.16}
\contentsline {section}{\numberline {A.17}Shell Ideas}{84}{section.A.17}
\contentsline {chapter}{\numberline {B}Technical Drawings}{87}{appendix.B}
\contentsline {chapter}{\numberline {C}Datasheets}{107}{appendix.C}
\contentsline {section}{\numberline {C.1}Datasheet \normalfont \textsc {Scubo}}{108}{section.C.1}
\contentsline {section}{\numberline {C.2}Linelaser}{109}{section.C.2}
\contentsline {section}{\numberline {C.3}VI-Sensor}{110}{section.C.3}
\contentsline {section}{\numberline {C.4}Intel NUC}{111}{section.C.4}
\contentsline {section}{\numberline {C.5}UDOO QUAD}{112}{section.C.5}
\contentsline {section}{\numberline {C.6}UI-1221LE-M-GL -- Stereo Camera}{113}{section.C.6}
\contentsline {section}{\numberline {C.7}UI-1251LE-M-GL -- Camera for 3D Modelling}{114}{section.C.7}
\contentsline {section}{\numberline {C.8}UI-3251LE-C-HQ -- Colour Front Camera}{115}{section.C.8}
\contentsline {section}{\numberline {C.9}Thrusters -- \normalfont \textsc {BlueRobotics T200}}{116}{section.C.9}
\contentsline {section}{\numberline {C.10}Threaded Nozzle}{119}{section.C.10}
\contentsline {section}{\numberline {C.11}C615 Logitech -- Webcam}{120}{section.C.11}
\contentsline {chapter}{\numberline {D}Testing Protocols}{121}{appendix.D}
\contentsline {section}{\numberline {D.1}\normalfont {\textsc {Pix4D}} by Night}{122}{section.D.1}
\contentsline {section}{\numberline {D.2}\normalfont {\textsc {Pix4D}} by Day with Lights}{123}{section.D.2}
\contentsline {section}{\numberline {D.3}\normalfont {\textsc {Pix4D}} by Day without Lights}{124}{section.D.3}
\contentsline {section}{\numberline {D.4}Hole Dimensions}{125}{section.D.4}
\contentsline {section}{\numberline {D.5}Ultrasonic}{126}{section.D.5}
\contentsline {section}{\numberline {D.6}Lego Testing}{127}{section.D.6}
