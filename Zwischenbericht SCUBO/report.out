\BOOKMARK [0][-]{chapter*.2}{Abstract}{}% 1
\BOOKMARK [0][-]{chapter*.3}{Preface}{}% 2
\BOOKMARK [0][-]{chapter*.4}{Symbols}{}% 3
\BOOKMARK [0][-]{chapter.1}{Introduction}{}% 4
\BOOKMARK [1][-]{section.1.1}{Scubo - our Robot}{chapter.1}% 5
\BOOKMARK [1][-]{section.1.2}{What is a Focus Project?}{chapter.1}% 6
\BOOKMARK [1][-]{section.1.3}{Goals and Vision}{chapter.1}% 7
\BOOKMARK [1][-]{section.1.4}{Key Features}{chapter.1}% 8
\BOOKMARK [1][-]{section.1.5}{Main Challenges}{chapter.1}% 9
\BOOKMARK [1][-]{section.1.6}{Conventions}{chapter.1}% 10
\BOOKMARK [1][-]{section.1.7}{The Team}{chapter.1}% 11
\BOOKMARK [1][-]{section.1.8}{Project Organization}{chapter.1}% 12
\BOOKMARK [1][-]{section.1.9}{Costs and Sponsoring}{chapter.1}% 13
\BOOKMARK [0][-]{chapter.2}{Design Process}{}% 14
\BOOKMARK [1][-]{section.2.1}{Guidelines by DRZ}{chapter.2}% 15
\BOOKMARK [1][-]{section.2.2}{Existing Underwater Robots}{chapter.2}% 16
\BOOKMARK [1][-]{section.2.3}{Requirements}{chapter.2}% 17
\BOOKMARK [1][-]{section.2.4}{Concept}{chapter.2}% 18
\BOOKMARK [1][-]{section.2.5}{Subsystems}{chapter.2}% 19
\BOOKMARK [1][-]{section.2.6}{Risk Analysis}{chapter.2}% 20
\BOOKMARK [0][-]{chapter.3}{Mechanical Design}{}% 21
\BOOKMARK [1][-]{section.3.1}{Overview}{chapter.3}% 22
\BOOKMARK [1][-]{section.3.2}{Central Unit}{chapter.3}% 23
\BOOKMARK [1][-]{section.3.3}{Exterior Parts}{chapter.3}% 24
\BOOKMARK [1][-]{section.3.4}{Weight and Balance}{chapter.3}% 25
\BOOKMARK [1][-]{section.3.5}{Sealing}{chapter.3}% 26
\BOOKMARK [1][-]{section.3.6}{FEM Check-Up}{chapter.3}% 27
\BOOKMARK [0][-]{chapter.4}{Electronics}{}% 28
\BOOKMARK [1][-]{section.4.1}{Cameras}{chapter.4}% 29
\BOOKMARK [1][-]{section.4.2}{Lights}{chapter.4}% 30
\BOOKMARK [1][-]{section.4.3}{Sensors}{chapter.4}% 31
\BOOKMARK [1][-]{section.4.4}{Central Computing Unit}{chapter.4}% 32
\BOOKMARK [1][-]{section.4.5}{VI Sensor}{chapter.4}% 33
\BOOKMARK [1][-]{section.4.6}{Laser}{chapter.4}% 34
\BOOKMARK [0][-]{chapter.5}{Software}{}% 35
\BOOKMARK [1][-]{section.5.1}{Operating System}{chapter.5}% 36
\BOOKMARK [1][-]{section.5.2}{Modelling}{chapter.5}% 37
\BOOKMARK [1][-]{section.5.3}{Computer Vision}{chapter.5}% 38
\BOOKMARK [0][-]{chapter.6}{Summary}{}% 39
\BOOKMARK [1][-]{section.6.1}{Conclusion}{chapter.6}% 40
\BOOKMARK [1][-]{section.6.2}{Outlook}{chapter.6}% 41
\BOOKMARK [0][-]{chapter*.30}{Bibliography}{}% 42
\BOOKMARK [0][-]{appendix.A}{Fundamentals}{}% 43
\BOOKMARK [1][-]{section.A.1}{Modelling Definitions}{appendix.A}% 44
\BOOKMARK [1][-]{section.A.2}{E-Mail transcript with Markus Inglin}{appendix.A}% 45
\BOOKMARK [1][-]{section.A.3}{Camera Evaluation}{appendix.A}% 46
\BOOKMARK [1][-]{section.A.4}{Volume Calculations}{appendix.A}% 47
\BOOKMARK [1][-]{section.A.5}{Full Analysis Central Unit}{appendix.A}% 48
\BOOKMARK [1][-]{section.A.6}{Weight Calculations}{appendix.A}% 49
\BOOKMARK [1][-]{section.A.7}{List of Requirements}{appendix.A}% 50
\BOOKMARK [1][-]{section.A.8}{FMEA}{appendix.A}% 51
\BOOKMARK [1][-]{section.A.9}{FMEA Description}{appendix.A}% 52
\BOOKMARK [1][-]{section.A.10}{List of Clamps}{appendix.A}% 53
\BOOKMARK [1][-]{section.A.11}{Weight and Balance}{appendix.A}% 54
\BOOKMARK [1][-]{section.A.12}{Gantt-Diagram}{appendix.A}% 55
\BOOKMARK [1][-]{section.A.13}{Functional Overview}{appendix.A}% 56
\BOOKMARK [1][-]{section.A.14}{Security Concept}{appendix.A}% 57
\BOOKMARK [1][-]{section.A.15}{Benefit Analysis}{appendix.A}% 58
\BOOKMARK [1][-]{section.A.16}{Estimation Battery Capacity}{appendix.A}% 59
\BOOKMARK [1][-]{section.A.17}{Shell Ideas}{appendix.A}% 60
\BOOKMARK [0][-]{appendix.B}{Technical Drawings}{}% 61
\BOOKMARK [0][-]{appendix.C}{Datasheets}{}% 62
\BOOKMARK [1][-]{section.C.1}{Datasheet Scubo}{appendix.C}% 63
\BOOKMARK [1][-]{section.C.2}{Linelaser}{appendix.C}% 64
\BOOKMARK [1][-]{section.C.3}{VI-Sensor}{appendix.C}% 65
\BOOKMARK [1][-]{section.C.4}{Intel NUC}{appendix.C}% 66
\BOOKMARK [1][-]{section.C.5}{UDOO QUAD}{appendix.C}% 67
\BOOKMARK [1][-]{section.C.6}{UI-1221LE-M-GL \205 Stereo Camera}{appendix.C}% 68
\BOOKMARK [1][-]{section.C.7}{UI-1251LE-M-GL \205 Camera for 3D Modelling}{appendix.C}% 69
\BOOKMARK [1][-]{section.C.8}{UI-3251LE-C-HQ \205 Colour Front Camera}{appendix.C}% 70
\BOOKMARK [1][-]{section.C.9}{Thrusters \205 BlueRobotics T200}{appendix.C}% 71
\BOOKMARK [1][-]{section.C.10}{Threaded Nozzle}{appendix.C}% 72
\BOOKMARK [1][-]{section.C.11}{C615 Logitech \205 Webcam}{appendix.C}% 73
\BOOKMARK [0][-]{appendix.D}{Testing Protocols}{}% 74
\BOOKMARK [1][-]{section.D.1}{Pix4D by Night}{appendix.D}% 75
\BOOKMARK [1][-]{section.D.2}{Pix4D by Day with Lights}{appendix.D}% 76
\BOOKMARK [1][-]{section.D.3}{Pix4D by Day without Lights}{appendix.D}% 77
\BOOKMARK [1][-]{section.D.4}{Hole Dimensions}{appendix.D}% 78
\BOOKMARK [1][-]{section.D.5}{Ultrasonic}{appendix.D}% 79
\BOOKMARK [1][-]{section.D.6}{Lego Testing}{appendix.D}% 80
