\chapter{Software}
\label{sec:software}
The following chapter gives an overview of \textsc{Scubo}'s software structure.\\
The requirement of \textsc{Scubo} included the implementation of an omnidirectional remote control and the gathering of images from several cameras around the robot. Since a lot of solutions are available online, the work consisted mainly of finding the right hard- and software fragments and assembling them to our needs.\\ \vspace{1cm}

\minitoc
\newpage



\section{Operating System}

\subsection{Robot Operating System (ROS)}
\label{sec:ros}
At the beginning of the project, we had to decide on the base of our software-framework. The choice was between the \textsc{LabVIEW} and ROS systems. \textsc{LabVIEW} is a graphical programming system and allows the user to create functional programs in the form of flowcharts and implement them on an embedded hardware device called myRIO. \textsc{LabVIEW} gained more and more popularity because of its intuitive user interface and basic method of programming. Many completed focus projects were built completely on this framework. ROS however, is specifically designed to control robotic components from a PC. It runs within another operating system which makes it a so-called meta operating system. A ROS system is composed of multiple nodes, which are programs by itself. These nodes communicate with each other through subscribing or publishing on a specific topic. It is an open source project and is therefore constantly maintained by many users. \\
\textsc{Scubo} runs on the ROS framework for several reasons. ROS allows to build a communication system over multiple machines that enables not to be bounded on one single hardware device. The subscriber/publisher model makes it very easy to merge independently coded subprograms, which are stored in different packages. It supports multiple programming languages and is a standard tool in the robotic industry with detailed documentation. It has been used in prior projects consisting computer vision with suitable results. Since we are also faced with complex problems in this direction this choice is consequential.

\subsection{Situation}
\label{sec:situation}
\textsc{Scubo} runs on two machines, all using \textsc{Linux} Ubuntu. The \textsc{Intel NUC} is built in for several bachelor theses about computer vision and should host ROS based programs as the master device. Since it does not provide the required interface for the rest of the equipment, it is connected to an UDOO QUAD. The UDOO QUAD is the main controller of the robot. It is connected to all eight thrusters and various sensors.

\subsubsection{Communication Structure}
\label{sec:communication_struct}
\begin{figure}[h]
	\includegraphics[width=\columnwidth]{images/remo/comm_struct.pdf}
	\caption{Communication Structure}
	\label{fig:communication_struct}
\end{figure}	
The camera network will be connected to the \textsc{Intel NUC} via an Universal Serial Bus (USB). It provides a simple solution to handle all camera connections and to minimize a tangled mess of cables by having the power supply and data transmission on one link. The four USB-Ports of the \textsc{Intel NUC} are expanded by two USB(3.0)-Hubs whereby one Hub is linked by the web-cams, second by the modular ports. Each web-cam is connected via USB2.0 and generates a data stream of maximal 60 MByte/s therefore one USB3.0 Port (500 MByte/s) is sufficient for all web-cams. The industrial and stereo camera are directly linked to \textsc{Intel NUC} via USB2.0. As for the UDOO QUAD, an analogue connection is required to read out the values of pressure, temperature, leak and battery voltage sensor. Serial Peripheral Interface (SPI) is a full duplex synchronous serial communication interface. It provides a fast data transmission in both direction therefore it is used to read the IMU data stream. Inter-Integrated Circuit (I$^2$C) in combination with a \textsc{Adafruit} 16-Channel 12-bit PWM/Servo Driver breakout-board will be used to expand the cable connection between the UDOO QUAD and the eight Electronic Speed Controls (ESC) (see Figure \ref{fig:communication_struct}).
% The data rate must be considered. \textsc{Scubo} is run by a NUC, which only provides two USB2.0 and three USB3.0 ports. \textbf{HIER RICHTIGE ZAHLEN VERWENDEN}In fact, \textsc{Scubo} will carry eight cameras. Therefore, the number of possible USB3.0 cameras is strictly limitated and the USB2.0 cameras must be combined in a USB hub.\textbf{HIER DATENBLATTLINK} At last, the cables of all external mounted cameras must fit into the cable gland.
\subsubsection{Communication between Machines}
\label{sec:comm_machine}
We defined the \textsc{Intel NUC} as the master, whereas the UDOO QUAD and the User Interface as slaves. The role of the master is to enable the communication between subscribers and publishers. Since all devices are spatial separated, we used \textquotedblleft\verb+ROS_MASTER_URI+\textquotedblright to enable communication between multiple machines. The microcontroller on the UDOO QUAD can be programmed using the \textsc{Arduino} Integrated Development Environment (IDE). By using the \textquotedblleft \verb+rosserial_arduino+\textquotedblright package, one can set up a interaction between microcontroller and microprocessor. It defines the \textsc{Arduino} to be a ROS node, which can actively publish and subscribe to defined topics. 

\subsection{Software Packages}
\label{sec:soft_pack}
Since \textsc{Scubo} consists of independent software modules such as the propulsion unit or sensor data measurement, we arrange them into following packages. 
\begin{itemize}
	\item \verb+control_system+: implementation of a stabilization controller;
	\item \verb+remote_control+: remapping of the user input to a control signal;
	\item \verb+sensor_monitoring+: data acquisition from all sensors;
	\item \verb+computer_vision+: camera calibration, mapping and localisation;
	\item \verb+camera_monitoring+: access to the camera network;
	\item \verb+virtual_reality+: development of the \textquotedblleft Unity\textquotedblright virtual reality environment.
\end{itemize}

\subsubsection{Version Control}
\label{sec:version_control}
Whenever multiple people are programming at the same time, it is crucial to have some form of version control to avoid unwanted settlements and deletion of codes. Therefore we set up a \textquotedblleft GIT\textquotedblright repository hosted by \textsc{BitBucket} to manage the source codes. This makes it possible to see a history of all made changes to the source code. This is beneficial, when errors occur and one wants to go back to an earlier version.

\section{Modelling}
\label{sec:modeling}
For the design of suitable control structures, it is very important to have a good model of the plant.\\
This section elaborates the modelling approach chosen and the corresponding mathematical formulations. The definitions of the skew symmetric matrix of a vector, passive rotation matrices, quaternions and further used concepts can be found in the Appendix \ref{data:modeldef}.


%\subsection{Notation}
%\label{sec:notation}
%Throughout this chapter the notation introduced here will be used. $P$ denotes an arbitrary point, $A$ and $B$ arbitrary moving frames and $I$ an earth-fixed inertial frame. Concerning vectors, the notation is best illustrated with an example: $_A\vec{r}_{BP}$ is the position vector of point $P$ w.r.t. $B$ expressed in frame $A$. Similarly, $_A\vec{\omega}_{IB}$ is the angular velocity of frame $B$ w.r.t. the inertial frame $I$ expressed in frame $A$.\\

\subsection{Kinematics}
\label{sec:kinematics}
The motion of a rigid body moving in space can be described by the velocity and angular rates of an attached body-fixed frame, denoted by $E$, w.r.t. to an inertial earth-fixed frame $I$. In order to have a short notation,  the vector of generalized velocities is introduced:
\begin{equation}
	_{_E}\vec{\theta} = \begin{bmatrix}_{_E}\vec{v}_{_{IE}}\\_{_E}\vec{\omega}_{_{IE}}\end{bmatrix}.
\end{equation}
In principle, position and orientation are obtained from integration of the generalized velocities. However, since \textsc{Scubo} is omnidirectional, a singularity-free parametrization is required. This is why a quaternion-based attitude representation was adopted. For notational simplicity, the vector of generalized coordinates is defined:
\begin{equation}
	\vec{\varphi} = \begin{bmatrix}_{_I}\vec{r}_{_{IE}}\\ q_{_{IE}}\end{bmatrix}.
\end{equation}
Obviously $\vec{\varphi}$ cannot be obtained by direct integration of $_{_E}\vec{\theta}$. Rather, the following transformation given by the Jacobian matrix $_{_{IE}}J_{\varphi}$ must first be applied:
\begin{align}
	\dot{\vec{\varphi}}  = _{_{IE}}{\mat{J}_\varphi} \cdot _{_E}\vec{\theta}, \qquad _{_{IE}}{\mat{J}_\varphi} &= \begin{bmatrix} \mat{R}_{_{IE}} & 0_{3\times 3} \\ 0_{4\times 3} & -1/2 \cdot \mat{\Xi} (q) \end{bmatrix}, \\ 
	\text{where} \quad \mat{\Xi}(q) &= \begin{bmatrix}-\breve{q}^T \\ q_0I_{3\times 3}+q^\times \end{bmatrix}.
\end{align}
%$\dot{\vec{\varphi}}  = _{IE}{J_\varphi} \cdot _E\vec{\theta}, \quad _{IE}{J_\varphi} = \begin{bmatrix} R_{IE} & 0_{3\times 3} \\ 0_{4\times 3} & -1/2 \cdot \Xi (q) \end{bmatrix}, \quad$ where $\quad \Xi(q) = \begin{bmatrix}-\breve{q}^T \\ q_0I_{3\times 3}+q^\times \end{bmatrix}$\\
In fact, an entire set of Jacobian matrices is needed for the modelling procedure, in order to relate velocities or forces/torques expressed in different frames to each other. The following list shows what the individual Jacobians are needed for and how to calculate them.
\begin{itemize}
	\item Jacobians transforming forces/torques acting at $C$ expressed in $E$ to generalized forces $\vec{\tau}$ expressed in $E$:
	\begin{align}
		\begin{bmatrix}_{_E}\vec{r}_{_{IC}} \\ _{_E}\vec{\omega}_{_{C}} \end{bmatrix} = \begin{bmatrix}_{_{EE}} \mat{J}_{_{P,CE}} \\ _{_{EE}} \mat{J}_{_{R,CE}} \end{bmatrix} \cdot _{_E}\vec{\theta}, &\quad _{_E}\vec{\tau} = \begin{bmatrix}_{_{EE}}\mat{J}_{_{P,CE}} \\ _{_{EE}}\mat{J}_{_{R,CE}} \end{bmatrix}^T \cdot \begin{bmatrix}_{_E}\vec{F}_{_{C}} \\ _{_{E}}\vec{\Gamma}_{_{C}} \end{bmatrix}\\
		_{_{EE}}{\mat{J}_{_{P,CE}}} &= \begin{bmatrix} \mat{I}_{3 \times 3} & - _{_E}\vec{r}_{_{EC}}^{\times} \end{bmatrix}\\
		_{_{EE}}{\mat{J}_{_{R,CE}}} &= \begin{bmatrix} 0_{3 \times 3} & - \mat{I}_{3 \times 3} \end{bmatrix}
	\end{align}
	Time derivatives:
	\begin{align}
		_{_{EE}}\dot{\mat{J}}_{_{P,CE}} &= \begin{bmatrix} _{_E}\vec{\omega}_{_{IE}}^\times & -( _{_E}\vec{\omega}_{_{IE}} \times _{_E}\vec{r}_{_{EC}})^\times \end{bmatrix}\\
		_{_{EE}}\dot{\mat{J}}_{_{R,CE}} &= \begin{bmatrix} 0_{3 \times 3} & _{_E}\vec{\omega}_{_{IE}}^\times \end{bmatrix}
	\end{align}
	\item Jacobians transforming forces/torques acting at $C$ expressed in $I$ to generalized forces $\vec{\tau}$ expressed in $E$:
	\begin{align}
		\begin{bmatrix}_{_I}\vec{r}_{_{IC}} \\ _{_I}\vec{\omega}_{_C} \end{bmatrix} = \begin{bmatrix}_{_{IE}} \mat{J}_{_{P,CE}} \\ _{_{IE}} \mat{J}_{_{R,CE}} \end{bmatrix} \cdot _{_E}\vec{\theta}, &\quad _{_E}\vec{\tau} = \begin{bmatrix}_{_{IE}} \mat{J}_{_{P,CE}} \\ _{_{IE}} \mat{J}_{_{R,CE}} \end{bmatrix}^T \cdot \begin{bmatrix}_{_I}\vec{F}_{_{C}} \\ _{_{I}} \vec{\Gamma}_{_C} \end{bmatrix}\\
		_{_{IE}}{\mat{J}_{_{P,CE}}} &= \begin{bmatrix} \mat{R}_{_{IE}} & -\mat{R}_{_{IE}} \cdot {}_{_E}\vec{r}_{_{EC}}^{\times} \end{bmatrix}\\
		_{_{IE}}{\mat{J}_{_{R,CE}}} &= \begin{bmatrix} 0_{3 \times 3} & \mat{R}_{_{IE}} \end{bmatrix}
	\end{align}
\end{itemize}

\subsection{Dynamics}
\label{sec:dynamics}
The dynamics of a rigid body moving in space can be described by a set of equations of motion (EoM), which relate the forces acting on the body to its acceleration. The EoM can be expressed in the inertial earth-fixed frame $I$ or a body-fixed frame $E$. In fact, any other frame could be chosen as well. However, when it comes to computational efficiency, it is usually best to choose a body-fixed frame, since otherwise the mass matrix $\mat{M}$ and the vector of centrifugal and Coriolis forces $\vec{b}$ become orientation dependent. On the other hand, the effect of forces which are constant in the inertial frame, such as gravity and buoyancy, will then be orientation dependent. In order to further simplify the equations, the origin of frame $E$ was chosen to lie a the geometrical centre of the main box.\\
The external effects acting on \textsc{Scubo} modelled are gravity, buoyancy, damping and added mass/inertia. The last effect is due to the fact that an accelerating body has to displace and accelerate the surrounding fluid as well.\\
These choices lead to the structure of the EoM given by the equation:\\
\begin{equation}\label{eq:eom}
	\left( {\mat{M}} + {\mat{M}_{_{A}}} \right) \cdot _{_E}\dot{\vec{\theta}}  + \vec{b} \left( _{_E}\vec{\theta} \right) + \vec{b}_{_A} \left( _{_E}\vec{\theta} \right) + \vec{D} \left( _{_E}\vec{\theta} \right) + \vec{g} \left( \vec{\varphi} \right) = _{_E}\vec{\tau}_{_{\rm{Thusters}}} + _{_E}\vec{\tau}_{_{\rm{Disturbances}}}
\end{equation}
The following list gives a detailed explanation of the individual terms and variables.
\begin{itemize}
	\item $\mat{M}$: Mass matrix. Given the mass of \textsc{Scubo} is $m$ and its moment of inertia w.r.t. the centre of mass $C$ expressed in frame $E$ is $\mat{_{_E}\Theta_{_C}}$, one obtains:
	\begin{align}
		\mat{M}=_{_{EE}}\mat{J}_{_{P,CE}}^T \cdot {m} \cdot {}_{_{EE}}\mat{J}_{_{P,CE}}+{}_{_{EE}}\mat{J}_{_{R,CE}}^T \cdot _{_E}\Theta_{_C} \cdot {}_{_{EE}}\mat{J}_{_{R,CE}}.
	\end{align}
	\item $\vec{b} \left( _{_E}\vec{\theta} \right)$: Vector of centrifugal and Coriolis forces. Computation:
	\begin{align*}
		\vec{b} \left( _{_E}\vec{\theta} \right) = _{_{EE}}\mat{J}_{_{P,CE}}^T \cdot {m} \cdot {}_{_{EE}}\dot{\mat{J}}_{_{P,CE}} \cdot {}_{_E}\vec{\theta} + {}_{_{EE}}\mat{J}_{_{R,CE}}^T \cdot {}_{_E}{\Theta _{_C}} \cdot {}_{_{EE}}\dot{\mat{J}}_{_{R,CE}} \cdot {}_{_E}\vec{\theta} + \dots
	\end{align*} \vspace{-20pt}
	\begin{align}
		\dots {}_{_{EE}}\mat{J}_{_{R,CE}}^T \cdot \left( {}_{_{EE}}{\mat{J}_{_{R,CE}}} \cdot {}_{_E}\vec{\theta} \right) \times \left( {}_{_E}{\Theta _{_C}} \cdot {}_{_{EE}}{\mat{J}_{_{R,CE}}} \cdot {}_{_E}\vec{\theta} \right)
	\end{align}
	\item $\vec{g} \left( \vec{\varphi} \right)$: Vector of potential forces, i.e., gravity and buoyancy. Given the acceleration due to gravity $g$, the
	Volume $V$ of Scubo, the density $\rho$ of water and the location $_E\vec{r}_{EB}$ of the centre of buoyancy, one obtains:
	\begin{align}
		\vec{g} \left( \vec{\varphi} \right) = {}_{_{IE}}\mat{J}_{_{P,CE}}^T \cdot \begin{bmatrix}	0 \\ 0 \\ +mg \end{bmatrix} + \begin{bmatrix}
		\mat{I}_{3\times 3} & -_{_E}\vec{r}_{_{EB}}^\times \\ 0_{3\times 3} & \mat{I}_{3\times 3} \end{bmatrix}^T \cdot \begin{bmatrix}	0 \\ 0 \\ - V\rho g \end{bmatrix}
	\end{align}
	\item $_{_E}\vec{\tau}_{_{\rm{Thrusters}}}$: Vector containing the forces and torques exerted by the thrusters. The eight thrusters $k=1,2,\dots,8$ have position $_{_E}\vec{r}_{_{\rm{Thruster},k}}$ w.r.t. the frame $E$ and exert an oriented force $_{_E}\vec{F}_{_{\rm{Thruster},k}}$ . This yields the generalized forces:
	\begin{align}
		_{_E}\vec{\tau}_{_{\rm{Thrusters}}} = \sum\limits_{k = 1}^8 \begin{bmatrix} \mat{I}_{3 \times 3} & - {}_{_E}\vec r_{_{\rm{Thruster},k}}^ \times \\ 0_{3 \times 3} & \mat{I}_{3 \times 3} \end{bmatrix}^T \cdot \begin{bmatrix} _{_E}{\vec F}_{_{\rm{Thruster},k}} \\ 0 \end{bmatrix}
	\end{align}
	\item $_{_E}\vec{\tau}_{_{\rm{Disturbances}}}$: Vector representing forces and torques created by disturbances such as currents or waves.
	\item $\mat{M}_{_A}$ and $\vec{b}_{_A}(_{_E}\vec{\theta})$: Mass matrix and vector of centrifugal and Coriolis forces related to added mass. Their calculation is similar to the one of $\mat{M}$ and $\vec{b}(_{_E}\vec{\theta})$. However, now three directional mass terms, $m_{_Ax}$, $m_{_Ay}$, $m_{_Az}$ and the moment of inertia tensor $diag\left(I_{_{A,xx}}, I_{_{A,yy}}, I_{_{A,zz}}\right)$ have to be considered. This diagonal structure implies that cross-coupling is neglected, but simplifies the parameter identification, since only six coefficients need to be determined. One then obtains:
	\begin{align}
		\mat{M}_{_A} = diag\left( {m_{A,x}},{m_{A,y}},{m_{A,z}},{I_{A,xx}},{I_{A,yy}},{I_{A,zz}} \right)
	\end{align}
	\begin{equation}
		\vec{b}_{_A}(_{_E}\vec{\theta}) = \begin{bmatrix}
		m_{A,x} \cdot \left( {qw - rv} \right) \\ 
		m_{A,y} \cdot \left( {ru - pw} \right) \\ 
		m_{A,z} \cdot \left( {pv - qu} \right) \\ 
		qr \cdot \left( I_{_{A,zz}} - I_{_{A,yy}} \right) \\ 
		rp \cdot \left( I_{_{A,xx}} - I_{_{A,zz}} \right) \\ 
		pq \cdot \left( I_{_{A,yy}} - I_{_{A,xx}} \right)
		\end{bmatrix}
	\end{equation}
	\item $\vec{D}(_{_E}\vec{\theta})$: Vector of damping forces. Since the calculation of the friction forces is rather difficult	(dependence on Reynolds number, flow conditions, surface geometry, etc.) the assumption of pure velocity dependence was made. Cross-coupling, which for instance occurs when driving curves, is completely neglected and furthermore, only first and second order terms are considered. These simplifications, as proposed by Antonelli\footnote{\cite{AntonelliAntonelli2014}, p. 39.} and Fossen\footnote{\cite{Fossen1994}, p. 43.}, lead to the structure shown below, which still requires the identification of 12 coefficients (denoted by $C$).\\
	\begin{equation}
		\vec{D}(_E\vec{\theta}) = \begin{bmatrix}
		C_{u1}u + C_{u2} \left| u \right|u \\ 
		C_{v1}v + C_{v2} \left| v \right|v \\ 
		C_{w1}w + C_{w2} \left| w \right|w \\ 
		C_{p1}p + C_{p2} \left| p \right|p \\ 
		C_{q1}q + C_{q2} \left| q \right|q \\ 
		C_{r1}r + C_{r2} \left| r \right|r
		\end{bmatrix}
	\end{equation}
	
\end{itemize}
\subsection{Simulink Model}
\label{sec:sim_model}
The EoM were implemented in \textsc{Simulink} as shown in Figure \ref{fig:sim_model}. The inputs are the thruster forces and the outputs the generalized coordinates $\vec{\varphi}$, the generalized velocities $_{_E}\vec{\theta}$ and the derivatives $_{_E}\dot{\vec{\theta}}$.
\begin{figure}[h]
	\includegraphics[width=\columnwidth]{images/yvain/sim_model.pdf}
	\caption{Simulink Model of \textsc{Scubo}}
	\label{fig:sim_model}
\end{figure}
\section{Computer Vision}
%\subsection{Virtual Reality IST DOPPELT DRIN!!!!!!}
%\label{sec:virtualreality2}
%
%
%The main goal of \textsc{Scubo} is to observe the development of the underwater world. We want to facilitate this process by implementing telepresence in our robot. Virtual reality can be used in telepresence in the form where a person wears a special form of glasses like Oculus Rift or the Osvr (see Section \ref{sec:vr}) and sees a scene of a virtual room. At the same time the person can apply head movements to explore and rotate its view in the virtual scene.
%Therefore \textsc{Scubo} has as a tool to connect the user to the underwater scene. By wearing virtual reality glasses which show the live-stream pictures of the cameras. This enables the user to feel himself diving in a submarine with the fishes and the corals. Not only scientist will have it easier to do their work, but the entertainment sector will profit as well. People that cannot scuba dive get excess to the underwater scene. \\ 

%Bilder

%There are two possibilities to implement virtual reality. Either the user only sees the pictures of the 6 cameras in his glasses or the person is present in a virtual room. After various meetings with the Innovation Center Virtual Reality group of the ETH we decided to design the inside of a virtual submarine. The user will only be able to apply head movements without walking in the room. This virtual room will be designed like a submarine that has 6 virtual windows. Each camera stream will be projected on to a window, whereby the pictures will remain in 2D. 

%\textbf{schreibe noch über fall-back solution die bilder bloss auf bildschrim anzuzeigen}



% http://www.wired.com/wp-content/uploads/2015/06/Oculus-Rift-2-1024x576.jpg
% http://cdn.slashgear.com/wp-content/uploads/2014/12/fold1.jpg
% http://www.pcgameshardware.de/screenshots/970x546/2015/01/Razer_OSVR__01_Aufmacher-pcgh.jpg
\subsection{Camera Calibration}
\label{sec:camcalibr}
When using cameras underwater, one might think that the main concern would be making the case waterproof. But when it comes to stereo vision, where one needs to match distinctive points of two different images and determine their distance and angle to the camera, another challenge arises. In order to calculate the real world location of a point in the image, the path of the light ray hitting the sensor must be  backtracked. And with the camera lens, air, plexiglas and water between the sensor and the object, a lot of refraction and distortion happens. The process of determining all the necessary parameters for the image rectification is called camera calibration. Software is available to determine these calibration parameters. This is done by moving a chessboard pattern in front of the camera, such that the program can analyse how the pattern gets distorted. During evaluation of the stereo vision approach, we performed this calibration process in air with the VI sensor and obtained parameters that produced good rectification of the images. During the upcoming semester, we will adapt this procedure for underwater use.

\subsection{3D Modelling}
\label{sec:pix4d}
In a very early stage of the project we evaluated the ability of the 3D reconstruction software \textsc{Pix4D}\footnote{\textsc{Pix4D} is a software that uses images to generate a 3D-model of the captured surrounding. It is meant to use in air.} to make a 3D-model of an object underwater. \\
For all of the tests we used a \textsc{GoPro Hero 3+ Black} with a dome and as an object we used a 3D printed coral.\\
We made the following three tests: 
% Aufzhälung oder einfach Bilder mit Bildunterschrift (Bilder werden noch nachgeliefert)
\begin{itemize}
	\item{at daylight with no lightsource\footnote{Full test protocol see Appendix \ref{data:pix4d_day}.},}
	\item{at daylight with diving lamp (1500 lm)\footnote{Full test protocol see Appendix \ref{data:pix4d_tag_licht}.},}
	\item{at night with diving lamp (1500 lm)\footnote{Full test protocol see Appendix \ref{data:pix4d_night}.}.}
\end{itemize}
Taking into account that a normal camera was used that was not calibrated to the conditions of water the two tests at daylight are considered a success since the reconstruction of the camera is recognizable (see Figure \ref{fig:pix4d} -- Reconstruction by day). The results imply that the software has troubles with the moving plants in the background. But due to the fact that the targeted corals are solid plants this problematic can be neglected.\\
The results of the test at night are useless, the light overexposes the coral\footnote{We did not make a reconstruction with \textsc{Pix4D} because no edges of the coral can be seen due to the overexposure.} (see Figure \ref{fig:pix4d} -- Reconstruction by night). Since the robot's main goal is not the use at night and coral reefs reflect light less than the 3D printing material this problematic can be neglected. It is important to mention that we did not recognize a substantial improvement of the 3D models when light sources were used. This due to the fact that enough daylight was available.
\begin{figure}[h]
	\begin{minipage}[t]{0.32\columnwidth}
		\centering 
		\includegraphics[width=\columnwidth]{images/leonie/pix4d_tag_keinLicht.pdf}
		\centering Reconstruction by Day, without Lights
	\end{minipage} \hfill
	\begin{minipage}[t]{0.32\columnwidth}
		\centering 
		\includegraphics[width=\columnwidth]{images/leonie/pix4d_tag_Licht.pdf}
		\centering Reconstruction by Day, with Lights
	\end{minipage} \hfill
	\begin{minipage}[t]{0.32\columnwidth}
		\centering 
		\includegraphics[height=2.3cm]{images/leonie/pix4d_nacht.pdf}
		\centering Attempted reconstruction by Night, with Lights
	\end{minipage}
	\caption{\textsc{Pix4d} Reconstructions under Different Circumstances}
	\label{fig:pix4d}
\end{figure}

\subsection{Virtual Reality}
\label{sec:virtualreality1}


A goal of \textsc{Scubo} is to observe the development of the underwater world. We want to facilitate this process by implementing telepresence in the robot. Virtual reality can be used in telepresence in the form where a person wears virtual reality glasses and sees a scene of a virtual room (see Figure \ref{fig:vr}). At the same time the person can apply head movements to explore and rotate its view in the virtual scene.
Therefore \textsc{Scubo} has as a tool to connect the user to the underwater world. By wearing virtual reality glasses which show the live-stream pictures of the cameras. This enables the user to feel himself diving in a submarine with the fishes and the corals. Not only scientist will have it easier to do their research, the entertainment sector will profit from this feature as well. People that cannot scuba dive get access to the underwater scene.\\
There are two possibilities to implement virtual reality. Either the user only sees the pictures of the six cameras in his glasses or the person is present in a virtual room. After various meetings with the Innovation Center Virtual Reality group of the ETH we decided to design the inside of a virtual submarine. The user will only be able to apply head movements without walking in the room. This virtual room will be designed like a submarine that has six virtual windows. Each camera stream will be projected on to a window, whereby the pictures will remain in 2D.
\begin{figure}[h]
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[width=\columnwidth]{images/ilyas/Disney1.jpg}
	\end{minipage}
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[width=\columnwidth]{images/ilyas/Disney2.jpg}
	\end{minipage}
	\caption[Example of Virtual Scene inside of Submarine]{Example of Virtual inside of Submarine\footnotemark}
	\label{fig:vr}
\end{figure}
\footnotetext{Pictures downloaded from \url{www.digitalmediafx.com} and \url{http://io9.gizmodo.com} (07.03.2016).}
\subsubsection{Virtual Reality Headset}\label{sec:vrhead}
To set up our virtual reality interface, a suitable headset is needed. The \textsc{Oculus Rift} is the device that dominates the virtual reality world. But since it is only available for pre-order, we chose an alternative headset called \textsc{Razer OSVR} (see Figure \ref{fig:osvr}). \textsc{OSVR} stands for Open Source Virtual Reality and is an open source software framework that developers can use to create their own virtual reality experience. The main headset is produced by the company \textsc{Razer}. The display itself offers a resolution of 960 x 1080 pixels per eye and allows for a complete $100^\circ$ field of view and is therefore an alternative to the \textsc{Oculus Rift}.\\

\begin{figure}[h]
	\centering
	\includegraphics[width=8cm]{images/jdiep/osvr}
	\caption[\textsc{OSVR} Virtual Reality Headset]{\textsc{OSVR} Virtual Reality Headset\footnotemark}
	\label{fig:osvr}
\end{figure}
\footnotetext{Picture downloaded from \url{http://www.pcgameshardware.de/screenshots/970x546/2015/01/Razer_OSVR__01_Aufmacher-pcgh.jpg} (07.03.2016).}
%\subsubsection{Available Headsets}\label{sec:vr}
%\textbf{dieser text lässt sich easy in punkten aufgliedern. als volltext ist es kaum lesbar}
%To set up our virtual reality interface, a suitable headset is needed. Therefore, we searched the market for existing state of the art products and compared them in price, availability and compatibility with the ''Unity'' software framework. Below, we listed our three favourite selection. As expected, the \textsc{Oculus Rift} is the device that dominates the virtual reality world. The system consists of a display for each eye, each with a resolution of 1080 x 1200 pixels, and integrated headphones. It has a field of vision of $110^{\circ}$ and thus fills out the human visual field entirely and creates a high impression of the virtual reality for the user. It supports several game engines besides ''Unity''. The first consumer product is available for pre-order and costs roughly 600 US-Dollar. Because of the huge demand, the first units are expected to ship in July at the earliest and therefore it is out of the question for our project scope. In contrast, the ''Google Cardboard'' offers a more convenient solution in price. It is an attachment made out of cardboard and two converging lenses and turns a regular smartphone displaying stereoscopic images into a virtual reality device. It is available online and costs approximately 20 US-Dollar. There exist software development kits which allows creating virtual reality experiences for Android or iOS applications using ''Unity''. The third headset is \textsc{Razer OSVR}. OSVR stands for Open Source Virtual Reality and is an open source software framework that developers can use to create their own virtual reality experience. The main headset is produced by the company \textsc{Razer} and is available for 300 US-Dollar. The display itself offers a resolution of 960 x 1080 pixels per eye and allows for a complete $100^\circ$ field of view. In conclusion, the OSVR platform offers a great alternative to the \textsc{Oculus Rift} for half the price and is therefore the logical choice for our project.\\
%\textbf{BILDER VON JOHANN MIT REFERENZ (LINKS ALS KOMMENTAR)}\\
% http://www.wired.com/wp-content/uploads/2015/06/Oculus-Rift-2-1024x576.jpg
% http://cdn.slashgear.com/wp-content/uploads/2014/12/fold1.jpg
% http://www.pcgameshardware.de/screenshots/970x546/2015/01/Razer_OSVR__01_Aufmacher-pcgh.jpg

