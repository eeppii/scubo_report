
\chapter{Software}
\label{sec:software}

The following chapter gives an overview of \textsc{Scubo}'s software structure.\\
The requirements included the implementation of an omnidirectional remote control and the gathering of images from several cameras around the robot. The description of the software is divided into subunits referring to the software parts running on the laptop, the \textsc{Intel} NUC and the \textsc{Arduino} DUE respectively.\\ 
\\
Please note that an introduction about how to properly start up the software of \textsc{Scubo} is found in Appendix \ref{data:startupInstruction}. Furthermore, the source code of the software discussed in this chapter is provided on the data storage medium associated with this report.\\

\vspace{1cm}
\minitoc
\newpage

\section{Robot Operating System (ROS)}
\label{sec:ros}
For the software-framework, ROS and \textsc{LabVIEW} were under consideration. \textsc{LabVIEW} is a graphical programming system and allows the user to create functional programs in the form of flowcharts and implement them on an embedded hardware device called myRIO. \textsc{LabVIEW} is popular because of its intuitive user interface and basic method of programming. Many completed focus projects were built completely on this framework. ROS however is specifically designed to control robotic components from a PC. It runs within another operating system which makes it a so-called middleware. A ROS system is composed of multiple nodes, which are programs itself. These nodes communicate with each other through subscribing or publishing on a specific topic. It is an open source project and is constantly maintained by many users.\\
\textsc{Scubo} runs on the ROS framework for several reasons. ROS allows to build a communication system over multiple machines that enables not to be bounded on one single hardware device. The subscriber/publisher model makes it simple to merge independently coded subprograms, which are stored in different packages. It supports multiple programming languages and is a standard tool in the robotic research community with detailed documentation. It has been used in prior projects related to computer vision and proved of value. %Since we are also faced with complex problems in this direction this choice is consequential.

\section{Communication}
\label{sec:communication}
In the following, the software structure, its functionality as well as its hardware components are presented.
\subsection{Computing Hardware}
\label{sec:situation}
As described in Section \ref{sec:computing_unit}, a \textsc{NUC} and an \textsc{Arduino} DUE are integrated in Scubo. The \textsc{NUC} is running \textsc{Linux} Ubuntu as operating system, which facilitates software development because of the equivalence to our laptop. The \textsc{NUC} functions as the master device in the local network, meaning it runs the ROS master process that manages the communication between different nodes of the software.

\subsection{Communication Structure}
\label{sec:communication_struct}
\begin{figure}[h]
	\includegraphics[width=\columnwidth]{images/remo/comm_struct.pdf}
	\caption{Communication Structure}
	\label{fig:communication_struct}
\end{figure}	
The camera network is connected to the \textsc{Intel NUC} via USB. It provides a simple solution to handle all camera connections and to minimize a tangled mess of cables by having the power supply and data transmission on one link. The four external USB-ports of the \textsc{Intel NUC} are extended by two internal ports and if needed by one via the M.2 bus\footnote{A universal port featuring e.g. USB and ethernet. A suitable adapter needs to be provided.}, resulting in up to seven direct USB ports. Furthermore, a USB3.0 hubs are installed. However, it reduce the overall speed to USB2.0 standard (60 MByte/s) as soon as a USB2.0 device is connected. Thus, it is not possible to use multiple of our USB2.0 webcams over one hub because they allocate nearly the whole bandwidth. Therefore, we use all the internal ports of the \textsc{NUC}. The stereo camera is directly linked to the \textsc{Intel NUC} via USB2.0. As for the \textsc{Arduino} DUE, an analogue connection is required to read out the values of  temperature, leak and battery voltage sensor. %Serial Peripheral Interface (SPI) is a full duplex synchronous serial communication interface. It provides a fast data transmission in both direction therefore it is used to read the IMU data stream. 
Inter-Integrated Circuit (I$^2$C) in combination with an \textsc{Adafruit} 16-Channel 12-bit PWM/Servo Driver breakout-board is used for the connection between the \textsc{Arduino} DUE and the eight Electronic Speed Controllers (see Figure \ref{fig:communication_struct}).
% The data rate must be considered. \textsc{Scubo} is run by a NUC, which only provides two USB2.0 and three USB3.0 ports. \textbf{HIER RICHTIGE ZAHLEN VERWENDEN}In fact, \textsc{Scubo} will carry eight cameras. Therefore, the number of possible USB3.0 cameras is strictly limitated and the USB2.0 cameras must be combined in a USB hub.\textbf{HIER DATENBLATTLINK} At last, the cables of all external mounted cameras must fit into the cable gland.

\subsubsection{Communication between Machines}
\label{sec:comm_machine}
We defined the \textsc{Intel NUC} as the master, whereas the \textsc{Arduino} DUE and the laptop as slaves. The role of the master is to enable the communication between subscribers and publishers. Since all devices are spatially separated, we used our local network and ROS' networking capabilities to enable communication between multiple machines. The microcontroller on the \textsc{Arduino} DUE can be programmed using the \textsc{Arduino} Integrated Development Environment (IDE). By using the \verb+rosserial_arduino+ package, one can set up an interaction between microcontroller and microprocessor. It defines the \textsc{Arduino} to be a ROS node, which can actively publish and subscribe to defined topics. 

\section{Software Packages}
\label{sec:soft_pack}

Since \textsc{Scubo} consists of independent software modules such as the sensor data measurement, we arranged them into packages, which were developed independently of each other:
\begin{itemize}
	\item User input: remapping of the steering commands to a control signal,
	\item Thruster actuation: control signal passed on to the thruster,
	\item Control: implementation of a controller,
	\item Camera monitoring: access to the camera network,
	\item Sensor monitoring: data acquisition from all sensors,
	\item Computer vision: camera calibration, mapping and localisation,
	\item Virtual reality: development of the \textsc{Unity} virtual reality environment.
\end{itemize}

\subsubsection{Version Control}
\label{sec:version_control}
We set up a \textsc{GIT} repository hosted by \textsc{BitBucket} to manage the source codes. This makes it possible to see a history of all changes made to ite, which is beneficial when errors occur and one wants to go back to an earlier version. Furthermore, it enabled parallel and independent development of the code.\\

The key points regarding the implementation of these software packages are discussed below.

\subsection{Key Points: Laptop}
\label{sec:software_laptop}

All required \textsc{ROS} nodes on the laptop can be started at once by executing the \verb+laptop_launchfile+ launchfile. A key element of the software running on the laptop is the controller. However, this topic is not introduced in this section. It will be covered in details in Section \ref{sec:control}.

\subsubsection{User Interaction}
\label{sec:user_interaction}
We are using a spacemouse by \textsc{3DConnexion} as our remote control joystick, since it offers an input signal with six degrees of freedom which correlates with the omnidirectionality of \textsc{Scubo}. Additionally, it has two physical buttons which we use to enable/disable the spacemouse and the controller. The rest of the interaction happens through terminal commands. This includes executing the launchfile \verb+laptop_launchfile+ to start the joystick driver, the controller and the monitoring node simultaneously. The user also has the possibility to dim the lamps by sending an integer value for brightness to the topic \verb+dimming_signal+. 

\subsubsection{Implementation of the Spacemouse}
 We use the node  \verb+spacenav_node+ as the link to \textsc{ROS} for the spacemouse. We found it to work stable\footnote{The output of the spacemouse has no outliers, which would be a problem for the thruster actuation.} if started through the \verb+no_deadband+ launchfile. However, we introduced a deadband of 10\% of the maximum magnitude to achieve that the thrusters are not actuated when the spacemouse is not moved. Without the deadband this would not have been possible, as the spacemouse never reaches zero magnitude again, if once moved after initialisation.\\

%\subsubsection{Feed forward control}
%As the controller of the submersible is developed in a bachelor thesis, we first implemented a feed forward control in the scope of the project. This included the allocation of the thrusters depending on the spacemouse input. We also implemented the \textquotedblleft dominant mode\textquotedblright, which is explained in Section \ref{sec:control_structure}. Furthermore, we implemented scaling factors for each rotational and translational movement.\\
%At the current state, the essential functions of the feed forward control are implemented in the controller package which is described in details in Section %\ref{sec:control_structure}.

\subsubsection{Camera View}
To view the image streams of the cameras, the \textsc{ROS} node \verb+image_view+ is used with the parameter \verb+image_transport+ set as \verb+compressed+ in order to reduce the amount of transmitted data. We encountered no bandwidth problems when streaming all six camera image streams simultaneously from the \textsc{Intel} NUC to the laptop. Note that this only holds for the wire transmission.\\

\subsubsection{Graphical User Interface (GUI)}
To improve the user friendliness of the interaction, we started to program a GUI on \textsc{Python} by using the \verb+tkinter+ library. This consists of a window with buttons to start and stop the main launchfile. The start button executes the command \verb+subprocess.Popen+ which opens the \verb+laptop_launchfile+ while keeping the GUI active at the same time. The stop button on the other hand terminates the running process by properly killing the corresponding nodes. Due to the lack of time, additional features like a scroll-button for the dimming and text field to output the sensor messages were not added.

\subsection{Key Points: \textsc{Intel} NUC}
\label{sec:software_nuc}

All different \textsc{ROS} nodes on the \textsc{NUC} can be started at once by executing the \verb+nuc_launchfile+ launchfile. Per default, this launchfile is automatically executed when the \textsc{Intel} NUC boots up.

\subsubsection{Camera Settings}
We discovered that the webcams allocate the whole USB2.0 bandwidth when running on the maximum resolution, even though these devices would need much less. In order to lower the allocated bandwidth, we run the webcams on 320$\times$240p instead of 800$\times$600p. Note that for telepresence application the limiting factor is the resolution of the VR glasses. The front camera can run on a higher resolution, namely 1280$\times$1024p as it is a USB3.0 device. With these settings, it is possible to run all cameras on 25-30 fps.\\

We used the configuration software \textsc{uEye Cockpit}\footnote{Available from the company \textsc{IDS}, developed to configure their \textsc{uEye} cameras. The software runs on \textsc{Windows}.} in order to determine the parameters named in Section \ref{sec:Cams} for the front camera. With the right choice of the resolution, the pixel clock as well as the frame rate and the exposure time, a fluent and high resolution image stream was possible. First, the resolution and the frame rate had to be defined, then the pixel clock was set such that the CPU of the camera was not fully claimed (around 60\%) and an acceptable range of the exposure time was still possible. Furthermore, the colour gains were adjusted to have an acceptable white balance\footnote{The gains regarding red, green and blue are adjusted as such that a white reference area is clearly seen as white by the watcher.}. Depending on the water, the blue gain can be adjusted to reduce the blueish tone in the images. The most important final parameters of the front camera are shown below.
\begin{table}[h]
	\begin{center}
		\begin{tabular}{|l|l|}
				\hline
			\textbf{Parameter} & \textbf{Value} \\ \hline
			Resolution & 1280$\times$1024p \\
			R,G,B gains & 6,0,25 \\
			Exposure& 15ms\\
			Auto exposure (if desired) & true\\
			Frame rate & 30 fps (25 fps to reduce bandwidth) \\
			Pixel clock & 64\\ \hline
		\end{tabular}
		\caption[Front Camera Parameter Values]{The Final Selection of the Parameter Regarding the Front Camera.}\vspace{1ex}
		\label{tab:front_param}
	\end{center}
\end{table}
\subsubsection{Camera Usage}

In order to control the webcams in \textsc{ROS} we used the \verb+usb_cam+ node. This node enabled us to set the basic parameters like resolution and  pixel format. The pixel format was set as \textquotedblleft mjpeg\textquotedblright{} in order to keep the bandwidth as low as possible.\\


Regarding the front camera, we used the \textsc{ROS} node  \verb+ueye_cam+. This enabled us to set various parameters, including the ones mentioned above.\\

\subsubsection{Bandwidth Limitation}
As described in Section \ref{sec:communication_struct} the following devices are connected to the \textsc{NUC}:
\begin{itemize}
	\item one USB3.0 front camera\footnote{For details see Section \ref{sec:cameras}.},
	\item five USB2.0 webcams\footnote{For details see Section \ref{sec:cameras}.},
	\item a USB2.0 stereo camera\footnote{For details see Section \ref{sec:vi}.},
	\item an \textsc{Arduino} DUE with USB2.0 connection\footnote{For details see Section \ref{subsec:raspi}.},
	\item further devices like IMU or custom sensors with USB2.0/3.0 connection.
\end{itemize}
The \textsc{NUC} provides the following connection ports:
\begin{itemize}
	\item four USB3.0 ports,
	\item two USB2.0 ports\footnote{Two internal USB2.0 connections, connected to two external USB2.0 port by manually added wires.},
	\item an internal M.2 M-key port.
\end{itemize}
As mentioned in Section \ref{sec:communication_struct}, we discovered bandwidth limitation to be a severe problem when connecting multiple USB devices to the \textsc{NUC}. We found the stability of the system to be dependent on the order of all connected devices. The following order resulted in a stable configuration and we recommend it to any user of \textsc{Scubo}:
\begin{itemize}
	\item the USB3.0 front camera is connected to a USB hub, which is then connected to the \textsc{NUC},
	\item two of the five USB2.0 webcams are connected to the two internal USB2.0 ports of the \textsc{Intel NUC}, two further webcams are connected directly to two of the USB3.0 ports of the \textsc{NUC}, the fifth webcam is connected to the USB hub, which also handles the front camera,
	\item the stereo camera is connected to the internal M.2 port,
	\item the \textsc{Arduino} DUE is connected directly to a USB3.0 port of the \textsc{NUC}.
\end{itemize}
If further devices are connected to the \textsc{NUC}, the following points have to be considered:
\begin{itemize}
	\item if a custom sensor is connected, it can be plugged into the USB hub together with the front camera instead of the webcam, this webcam must then be changed to a second USB hub together wit the \textsc{Arduino}. Note that this might limit the data transmission of the running \textsc{Arduino} program,
	\item if an IMU should be read in over an USB port\footnote{The built in IMU in \textsc{Scubo} is read in over the \textsc{Arduino}. However, for testing reasons another IMU might be requested.} or the stereo camera is connected for testing reasons regarding computer vision algorithms, all webcams should be unplugged to ensure full bandwidth availability.
\end{itemize}

Nevertheless, if the user should run into problems based on bandwidth limitations in any case, we recommend to unplug the webcams first as these USB2.0 devices are the most limiting factor. \\

\subsection{Key Points: \textsc{Arduino} DUE}
\label{sec:software_arduino}

The final version of the \textsc{Arudino} programming code can be used by uploading the \verb+arduino_codefile+ onto the \textsc{Arduino} DUE.

\subsubsection{Message Rate}
The \textsc{Arduino} DUE is not capable of multithreading\footnote{Running multiple processes simultaneously.}. Instead, it has to run its subscribing and publishing processes one by another. We introduced a software interrupt with \verb+millis()+ to read out the sensor signals in 10 seconds intervals, as these commands do not need to be run at high frequency. Furthermore, this was necessary as low priority sensor monitoring delayed critical processes such as the thruster control, which has to respond to user commands at high frequency. To avoid charge exchange at the multiplexer\footnote{Device that selects one of several anlog or digital input signals and forwards the selected input into a single line, Wikipedia.} at every readout, the function \verb+analogRead()+ is used twice with a delay in between, while the initialisation with a variable happens not until the second callout. The programming Code \ref{code:analog_read} shows this implementation.\\

\begin{lstlisting} [caption=Readout of an Analogue Pin, label=code:analog_read]
// avoid charge exchange
analogRead(analogPin);
delay(10);
sensor_value=analogRead(analogPin);

\end{lstlisting}

\subsubsection{Thruster Tuning}
%For initial thruster tests we controlled them using pwm-pins on the \textsc{Arduino}. The according function of the standard \textsc{Arduino} library was used. In this configuration the actuation signal is formulated in milliseconds and the thruster had no deadband\footnote{A deadband in the sense of they do not move for multiple different actuation signals.}.
In the final configuration we actuate the thrusters via I$^2$C pins connected to a breakout-board as described above. This implementation requested to define the signals pulse-width in bits. We discovered that this introduces a certain deadband\footnote{A deadband in the sense of the thruster do not move for multiple different actuation signals.} to the thrusters. Furthermore, this deadband changed slightly as soon as a hardware component (e.g. breakout-board, \textsc{Arduino}) was changed. This forced us to manually locate the deadband for each thruster and then link its resulting actuation interval to the spacemouse. Namely, in the \textsc{Arduino} programming code, the function \verb+pwm.setPWM(channel_nr,0, thruster_signal)+ must be called for different values of \verb+thruster_signal+. Then, the highest and lowest signal must be determined which actuates the individual thruster. Note that \verb+channel_nr+ defines which thruster is actuated.

\subsubsection{Pressure Sensor}
Knowing the water pressure at the outside of \textsc{Scubo} allows an accurate estimate of the current depth. We use a digital pressure sensor (data sheet in Appendix \ref{data:pressure}) that has a range of 0 to 6 bar and is connected to the \textsc{Arduino} DUE via I$^{2}$C interface. Aside from precise measurements, this has the advantage that a sensor reading takes less time than an analogue read, namely 3 milliseconds\footnote{We use an oversampling ratio (OSR) of 1024. A larger OSR results in higher precision but longer conversion time.} at most. This allows us to read the pressure at a rate of 100 Hz without interfering with the thruster control. The depth controller is also running at 100 Hz.\\
Only once during startup of the \textsc{Arduino}, ten calibration parameters are read from the sensor. Those 10-bit and 14-bit signed integers are converted to common 16-bit integers and stored for subsequent measurements. From the read-in values of the sensor the pressure value can be calculated by using the calibration parameters. It has the unit bar and is published to the topic \verb+pressure_signal+. The typical resolution is 0.003 \%span, that is 0.18 mbar for the range of our sensor and equals 1.8 mm difference in depth. The code for calibration and pressure reading can be found within the \verb+arduino_codefile+.

\section{Computer Vision}
\label{compVis}
Since the use of GPS signals under water is not possible, computer vision algorithms were used for the localisation of the robot. For collision avoidance, computer vision can be used as well.

\subsection{Localisation}
\label{sec:vilocal}
A VI-Sensor consists of a stereoscopic global shutter camera and an IMU. The use of stereo cameras combined with an IMU for perception is gaining popularity in robotics, although the implementation is ambitious. By comparing the images of a triggered stereo camera, the distance of visible features can be estimated. We use existing software from the ASL called ROVIO. This software estimates the state and the path of the robot by tracking so called features over time and combining this data with IMU measurements. The main challenge is to adapt the whole process for underwater application, which is done in the BT of Timo Str\"assle and Reto Bischoff.\\

\subsection{Collision Avoidance} 
\label{sec:vicoll}


\textsc{Scubo} is equipped with the necessary hardware for collision avoidance. Three bachelor theses are concerned with the software implementation of an offline collision avoidance system. The first one (BT of Timo Str\"assle and Reto Bischoff) adresses the task of generating a point cloud\footnote{A point cloud is a set of data points representing positions of features.} by post-processing recordings of a stereo camera. The second one (BT of Johann Diep) considers the implementation of a path planning algorithm which generates a collision-free trajectory using the point cloud. In the third one (BT of Ilyas Besler) the controller needed to follow this trajectory is designed. Not covered by the theses and the project are the connections between point cloud generation, path planning algorithm and trajectory controller. 

The long term goal is to enable real-time object detection, path planning and obstacle avoidance. 


\subsection{Camera Calibration}
\label{sec:camcalibr}
In order to calculate the real world location of a point in an image, the path of the light ray hitting the sensor must be backtracked. The camera lens, air, acrylic glas and water between the sensor and the object lead to refraction and distortion. The process of determining all the necessary parameters for the image rectification is called camera calibration. Software is available to determine these parameters for in-air usage. This is done by moving a chessboard pattern in front of the camera, such that the program can analyse how the pattern gets distorted. During evaluation of the stereo vision approach, we performed this calibration process in air with the VI sensor and obtained parameters that produced good rectification of the images. The adaption for the underwater use is covered in the BT of Leonie Traffelet and Thomas Eppenberger and is therefore not further discussed in this report.

\subsection{3D Modelling}
\label{sec:pix4d}
In a very early stage of the project we evaluated the ability of the 3D reconstruction software \textsc{Pix4D}\footnote{\textsc{Pix4D} is a software that uses images to generate a 3D-model of the captured surrounding. It is meant to use in air.} to make a 3D-model of an object underwater. \\
For all of the tests we used a \textsc{GoPro Hero 3+ Black} with a dome and as object a 3D printed coral.\\
We made the following three tests: 
\begin{itemize}
	\item{at daylight with no lightsource\footnote{Full test protocol see Appendix \ref{data:pix4d_day}.},}
	\item{at daylight with diving lamp (1500 lm)\footnote{Full test protocol see Appendix \ref{data:pix4d_tag_licht}.},}
	\item{at night with diving lamp (1500 lm)\footnote{Full test protocol see Appendix \ref{data:pix4d_night}.}.}
\end{itemize}
Taking into account that a normal camera with a dome was used instead of using a proper underwater calibration, the two tests at daylight are considered a success since the reconstruction of the camera is recognisable (see Figure \ref{fig:pix4d} -- Reconstruction by day) and namely, 80\% of the corals arms could be regained in the 3D model. The results imply that the software has troubles with the moving plants in the background. However, due to the fact that the targeted corals are solid, this problematic can be neglected.\\
The results of the test at night are useless, the light overexposes the coral\footnote{We did not make a reconstruction with \textsc{Pix4D} because no edges of the coral can be seen due to the overexposure.} (see Figure \ref{fig:pix4d} -- Reconstruction by night). Since the robot's main goal is not the use at night and coral reefs reflect light less than the 3D printing material, this problematic can be neglected for our purposes. It is important to mention that we did not recognize a substantial improvement of the 3D models when light sources were used. This due to the fact that enough daylight was available. \\
With these experiments we showed that 3D reconstruction with a photogrammetry approach underwater is possible with a program made for the application in air. Therefore, we did not need to look for alternative 3D modelling approaches.
\begin{figure}[h]
	\begin{minipage}[t]{0.32\columnwidth}
		\centering 
		\includegraphics[width=\columnwidth]{images/leonie/pix4d_tag_keinLicht.pdf}
		\centering Reconstruction by Day, without Lights
	\end{minipage} \hfill
	\begin{minipage}[t]{0.32\columnwidth}
		\centering 
		\includegraphics[width=\columnwidth]{images/leonie/pix4d_tag_Licht.pdf}
		\centering Reconstruction by Day, with Lights
	\end{minipage} \hfill
	\begin{minipage}[t]{0.32\columnwidth}
		\centering 
		\includegraphics[height=2.3cm]{images/leonie/pix4d_nacht.pdf}
		\centering Attempted reconstruction by Night, with Lights
	\end{minipage}
	\caption{\textsc{Pix4d} Reconstructions under Different Circumstances}
	\label{fig:pix4d}
\end{figure}

\section{Virtual Reality}
\label{sec:virtualreality1}


One vision of \textsc{Scubo} is to get the user closer to the stunning underwater world. Therefore, we chose to implement telepresence by virtual reality.\footnote{The Innovation Centre Virtual Reality group at ETH confirmed the feasibility.} \\
This is achieved through virtual reality glasses with help of which a person sees a scene of a virtual room (inspired by Figure \ref{fig:vr}). The virtual room represents the inside of \textsc{Scubo} and the user can look out of the \textquotedblleft windows\textquotedblright{} of the submersible robot. In each of these six windows the corresponding camera stream will be projected. By applying head movements this person can explore and rotate its view in this scene.  \\


%After various meetings with the Innovation Center Virtual Reality group of the ETH we decided to design the inside of a virtual submarine. This room has six windows and in each of these the corresponding camera-streams will be projected. 

We wanted the user to have the feeling to be inside of \textsc{Scubo} right in between corals and fishes. That way, our robot enters the entertainment sector. People that cannot scuba dive get access to the underwater world in a way not only the nature but also the used technology fascinates. 


%We wanted to let the user feel himself diving with the robot underwater together with the fishes and the corals. That way not only scientists will have it easier to do their research by seeing what is happening underwater, the entertainment sector will profit from this feature as well. People that cannot scuba dive get access to the underwater world.

% We want to facilitate this process by implementing telepresence in the robot through virtual reality Virtual reality can be used in telepresence in the form where a person wears virtual reality glasses and sees a scene of a virtual submarine (see Figure \ref{fig:vr}). 

%At the same time the person can apply head movements to explore and rotate its view in the this scene,

%by wearing virtual reality glasses which show the live-stream pictures of the cameras. This enables the user to feel himself diving in a submarine with the fishes and the corals. Not only scientist will have it easier to do their research, the entertainment sector will profit from this feature as well. People that cannot scuba dive get access to the underwater world.\\
%After various meetings with the Innovation Center Virtual Reality group of the ETH we decided to design the inside of a virtual submarine. The user will only be able to apply head movements without walking in the room. This virtual room will be designed like a submarine that has six virtual windows. Each camera stream will be projected on to a window, whereby the pictures will remain in 2D.
\begin{figure}[h]
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[width=\columnwidth]{images/ilyas/Disney1.jpg}
	\end{minipage}
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[width=\columnwidth]{images/ilyas/Disney2.jpg}
	\end{minipage}
	\caption[Inspiration for Virtual Scene inside of Submarine]{Inspiration for Virtual Scene inside of Submarine\footnotemark}
	\label{fig:vr}
\end{figure}
\footnotetext{\url{www.digitalmediafx.com} and \url{http://io9.gizmodo.com}, (07.03.2016).}
\subsection{Virtual Reality Headset}\label{sec:vrhead}
To set up our virtual reality interface, a suitable headset was needed. The \textsc{Oculus Rift} is the device that dominates this market. However, since it was only available for pre-order, we chose an alternative headset called \textsc{Razer OSVR}\footnote{\textsc{OSVR} stands for Open Source Virtual Reality and is an open source software framework that developers can use to create their own virtual reality experience. It is produced by the company \textsc{Razer}.} (see Figure \ref{fig:osvr}). The display itself offers a resolution of 960 x 1080 pixels per eye and allows a $100^\circ$ field of view.\\

\begin{figure}[h]
	\centering
	\includegraphics[width=8cm]{images/jdiep/osvr}
	\caption[\textsc{OSVR} Virtual Reality Headset]{\textsc{OSVR} Virtual Reality Headset\footnotemark}
	\label{fig:osvr}
\end{figure}
\footnotetext{\url{http://www.pcgameshardware.de/screenshots/970x546/2015/01/
	Razer_OSVR__01_Aufmacher-pcgh.jpg}, (07.03.2016).}
\subsection{Game Engine: \textsc{Unity}} \label{sec:vrunity}
A software called \textsc{Unity}\footnote{Developed by \textsc{Unity Technologies}.} was used to design this virtual submarine. \textsc{Unity} is a cross-platform game engine and used to develop video games for PC or consoles. It provided us with the required options to assemble a room and connect it to the \textsc{OSVR} glasses. 

%The slogan of \textsc{Unity Technologies} says:
%\footnotetext{\url{http://www.pcgameshardware.de/screenshots/970x546/2015/01/Razer_OSVR__01_Aufmacher-pcgh.jpg}, (07.03.2016).}
%\begin{center}
%	\textit{\textquotedblleft You can create any 2D or 3D game with Unity. You can make it with ease, you can make it highly-optimized and beautiful, and you can deploy it with a click to more platforms than you have fingers and toes. What's more you can use Unity's integrated services to speed up your development process optimize your game, connect with an audience, and achieve success.	\textquotedblright}
%\end{center}

\subsubsection{Virtual Submarine} \label{sec:vrsubmarine}
The virtual submarine was built with the help of an asset called \textquotedblleft Sci-fi Kit\textquotedblright{} which is bought from the \textsc{Unity} Asset store. It contains so called Prefabs of already designed blocks that allow assembling a room with six windows as seen in Figure \ref{fig:unity}.

\begin{figure}[h]
	\centering
	\includegraphics[width=12.5cm]{images/ilyas/unity}
	\caption{\textsc{Unity} Interior Design of Virtual Submarine}
	\label{fig:unity}
\end{figure}

The integration of the webcam streams is accomplished by scripts programmed in \verb+C#+ assigned to the planes of the virtual windows. \textsc{Unity} uses this scripts as an instruction of behaviour for the gameobjects, namely implement the image streams as textures.\\
%Therewith, for \textsc{Unity} to recognise the six cameras and distribute them correctly to the corresponding windows a special code was written.
Currently \textsc{Unity} is only fully supported on \textsc{Windows} OS. 
For first tests, we included webcams into \textsc{Unity} which were directly plugged into the machine the program was run on. All image streams could be projected without any delay. However, in the final setup the webcams are connected to the \textsc{Intel NUC}, which runs on \textsc{Linux} Ubuntu. Hence the challenge was to transmit the image streams from one operating system to the other and include it into \textsc{Unity} without delay and quality loss.\\

\subsection{Image Transport}


At the moment, the image streams have to be stored on the \textsc{Intel} NUC first. In post-processing, the data has to be converted to a \textsc{Unity} compatible format, namely \textquotedblleft mp4\textquotedblright. Then the image streams cam be read in by \textsc{Unity}. This solution presumes that the user can only see post-processed images and is therefore not able to steer the robot at the same time. The precise procedure can be seen in Figure \ref{fig:postprocess}.\\

The two most promising approaches how to be able to project real time image streams onto the windows in the virtual submarine are discussed below:\\
The first attempt is to share the image streams to a local network where each stream is published on a URL link in \textquotedblleft OGG Theora\textquotedblright{} format. To do so, either the ROS node \verb+web_video_server+\footnote{We successfully tested the ROS node: Six video streams were fluent and without a delay larger than 1 s.} or the software \textsc{VLC Media Player} can be used. \textsc{Unity} is currently not capable of including live streams from a URL link, only complete video files. As soon as \textsc{Unity} supports live streams, this issue would be solved.\\
Another attempt is to access the webcams on the \textsc{Intel NUC} directly via SSH. As long as the two devices run on the same OS this is possible. For this solution, either \textsc{Unity} must successfully run on \textsc{Ubuntu} or another \textsc{Ubuntu}-based and VR-glasses compatible editor could be used. This solution would reduce the delay and would not require a conversion of the image stream format.\\

\begin{figure}[h]
	\centering
	\includegraphics[width=12.5cm]{images/ilyas/postprocessing.pdf}
	\caption{Procedure Camera Implementation}
	\label{fig:postprocess}
\end{figure}
%\subsubsection{Available Headsets}\label{sec:vr}
%\textbf{dieser text lässt sich easy in punkten aufgliedern. als volltext ist es kaum lesbar}
%To set up our virtual reality interface, a suitable headset is needed. Therefore, we searched the market for existing state of the art products and compared them in price, availability and compatibility with the ''Unity'' software framework. Below, we listed our three favourite selection. As expected, the \textsc{Oculus Rift} is the device that dominates the virtual reality world. The system consists of a display for each eye, each with a resolution of 1080 x 1200 pixels, and integrated headphones. It has a field of vision of $110^{\circ}$ and thus fills out the human visual field entirely and creates a high impression of the virtual reality for the user. It supports several game engines besides ''Unity''. The first consumer product is available for pre-order and costs roughly 600 US-Dollar. Because of the huge demand, the first units are expected to ship in July at the earliest and therefore it is out of the question for our project scope. In contrast, the ''Google Cardboard'' offers a more convenient solution in price. It is an attachment made out of cardboard and two converging lenses and turns a regular smartphone displaying stereoscopic images into a virtual reality device. It is available online and costs approximately 20 US-Dollar. There exist software development kits which allows creating virtual reality experiences for Android or iOS applications using ''Unity''. The third headset is \textsc{Razer OSVR}. OSVR stands for Open Source Virtual Reality and is an open source software framework that developers can use to create their own virtual reality experience. The main headset is produced by the company \textsc{Razer} and is available for 300 US-Dollar. The display itself offers a resolution of 960 x 1080 pixels per eye and allows for a complete $100^\circ$ field of view. In conclusion, the OSVR platform offers a great alternative to the \textsc{Oculus Rift} for half the price and is therefore the logical choice for our project.\\
%\textbf{BILDER VON JOHANN MIT REFERENZ (LINKS ALS KOMMENTAR)}\\
% http://www.wired.com/wp-content/uploads/2015/06/Oculus-Rift-2-1024x576.jpg
% http://cdn.slashgear.com/wp-content/uploads/2014/12/fold1.jpg
% http://www.pcgameshardware.de/screenshots/970x546/2015/01/Razer_OSVR__01_Aufmacher-pcgh.jpg

