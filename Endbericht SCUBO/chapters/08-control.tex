\chapter{Modelling and Control}
\label{sec:modeling_and_control}
This chapter covers the control engineering part of project \textsc{Scubo}. First, the modelling approach is described along with the mathematical equations. Next, a list of the numerical parameter values used for the model is given. Finally, the control structure, which was implemented in ROS/C++, is depicted.

%The chapter concludes with an outlook on how the model and the controllers could be improved.\\
%Due to time constraints and issues with \textsc{Rovio}, almost no experimental data on the performance of the controllers was available at the end of the project. Therefore, only simulation results are given. Nevertheless, the entire control structure is implemented on \textsc{Scubo} and ready to be used.\\
\vspace{1cm}
\minitoc
\newpage

\section{Modelling}
\label{sec:modeling}
For the design of a suitable control structure, it was essential to have a physical model of \textsc{Scubo}. This allowed us to perform testing and tuning on the computer and to go diving with the real system for final adjustments only.\\
This section elaborates the modelling approach chosen and the corresponding mathematical formulations.\footnote{Source: The lecture \textquotedblleft Robot Dynamics\textquotedblright{} taught by Prof. Dr. M. Hutter, Prof. Dr. R. Siegwart and T. Stastny. Website: \url{http://www.asl.ethz.ch/education/lectures/robot_dynamics.html}, (12.06.2016).} The definitions of the skew symmetric matrix of a vector, passive rotation matrices, quaternions and further used concepts can be found in Appendix \ref{data:modeldef}.

\subsection{Kinematics}
\label{sec:kinematics}
The motion of a rigid body moving in space can be described by the velocity and angular rates of an attached body-fixed frame, denoted by $E$, w.r.t. an inertial earth-fixed frame $I$. In order to have a short notation,  the vector of generalized velocities is introduced:
\begin{equation}
	_{_E}\vec{\theta} = \begin{bmatrix}_{_E}\vec{v}_{_{IE}}\\_{_E}\vec{\omega}_{_{IE}}\end{bmatrix}.
\end{equation}
In principle, position and orientation are obtained from integration of the generalized velocities. However, since \textsc{Scubo} is omnidirectional, a singularity-free parametrization is required. This is why a quaternion-based attitude representation was adopted. For notational simplicity, the vector of generalized coordinates is defined:
\begin{equation}
	\vec{\varphi} = \begin{bmatrix}_{_I}\vec{r}_{_{IE}}\\ q_{_{IE}}\end{bmatrix}.
\end{equation}
This vector cannot be obtained by direct integration of $_{_E}\vec{\theta}$. Rather, the following transformation given by the Jacobian matrix $_{_{IE}}J_{\varphi}$ must first be applied:
\begin{align}
	\dot{\vec{\varphi}}  = _{_{IE}}{\mat{J}_\varphi} \cdot _{_E}\vec{\theta}, \qquad _{_{IE}}{\mat{J}_\varphi} &= \begin{bmatrix} \mat{R}_{_{IE}} & 0_{3\times 3} \\ 0_{4\times 3} & -1/2 \cdot \mat{\Xi} (q) \end{bmatrix}, \\ 
	\text{where} \quad \mat{\Xi}(q) &= \begin{bmatrix}-\breve{q}^T \\ q_0I_{3\times 3}+q^\times \end{bmatrix}.
\end{align}
%$\dot{\vec{\varphi}}  = _{IE}{J_\varphi} \cdot _E\vec{\theta}, \quad _{IE}{J_\varphi} = \begin{bmatrix} R_{IE} & 0_{3\times 3} \\ 0_{4\times 3} & -1/2 \cdot \Xi (q) \end{bmatrix}, \quad$ where $\quad \Xi(q) = \begin{bmatrix}-\breve{q}^T \\ q_0I_{3\times 3}+q^\times \end{bmatrix}$\\
In fact, an entire set of Jacobian matrices is needed for the modelling procedure, in order to relate velocities or forces/torques expressed in different frames to each other. The following list shows what the individual Jacobians are needed for and how to calculate them.
\begin{itemize}
	\item Jacobians transforming forces/torques acting at $C$ (CoM) expressed in $E$ to generalized forces $\vec{\tau}$ expressed in $E$:
	\begin{align}
		\begin{bmatrix}_{_E}\vec{r}_{_{IC}} \\ _{_E}\vec{\omega}_{_{C}} \end{bmatrix} = \begin{bmatrix}_{_{EE}} \mat{J}_{_{P,CE}} \\ _{_{EE}} \mat{J}_{_{R,CE}} \end{bmatrix} \cdot _{_E}\vec{\theta}, &\quad _{_E}\vec{\tau} = \begin{bmatrix}_{_{EE}}\mat{J}_{_{P,CE}} \\ _{_{EE}}\mat{J}_{_{R,CE}} \end{bmatrix}^T \cdot \begin{bmatrix}_{_E}\vec{F}_{_{C}} \\ _{_{E}}\vec{\Gamma}_{_{C}} \end{bmatrix}\\
		_{_{EE}}{\mat{J}_{_{P,CE}}} &= \begin{bmatrix} \mat{I}_{3 \times 3} & - _{_E}\vec{r}_{_{EC}}^{\times} \end{bmatrix}\\
		_{_{EE}}{\mat{J}_{_{R,CE}}} &= \begin{bmatrix} 0_{3 \times 3} & - \mat{I}_{3 \times 3} \end{bmatrix}
	\end{align}
	Time derivatives:
	\begin{align}
		_{_{EE}}\dot{\mat{J}}_{_{P,CE}} &= \begin{bmatrix} _{_E}\vec{\omega}_{_{IE}}^\times & -( _{_E}\vec{\omega}_{_{IE}} \times _{_E}\vec{r}_{_{EC}})^\times \end{bmatrix}\\
		_{_{EE}}\dot{\mat{J}}_{_{R,CE}} &= \begin{bmatrix} 0_{3 \times 3} & _{_E}\vec{\omega}_{_{IE}}^\times \end{bmatrix}
	\end{align}
	\item Jacobians transforming forces/torques acting at $C$ expressed in $I$ to generalized forces $\vec{\tau}$ expressed in $E$:
	\begin{align}
		\begin{bmatrix}_{_I}\vec{r}_{_{IC}} \\ _{_I}\vec{\omega}_{_C} \end{bmatrix} = \begin{bmatrix}_{_{IE}} \mat{J}_{_{P,CE}} \\ _{_{IE}} \mat{J}_{_{R,CE}} \end{bmatrix} \cdot _{_E}\vec{\theta}, &\quad _{_E}\vec{\tau} = \begin{bmatrix}_{_{IE}} \mat{J}_{_{P,CE}} \\ _{_{IE}} \mat{J}_{_{R,CE}} \end{bmatrix}^T \cdot \begin{bmatrix}_{_I}\vec{F}_{_{C}} \\ _{_{I}} \vec{\Gamma}_{_C} \end{bmatrix}\\
		_{_{IE}}{\mat{J}_{_{P,CE}}} &= \begin{bmatrix} \mat{R}_{_{IE}} & -\mat{R}_{_{IE}} \cdot {}_{_E}\vec{r}_{_{EC}}^{\times} \end{bmatrix}\\
		_{_{IE}}{\mat{J}_{_{R,CE}}} &= \begin{bmatrix} 0_{3 \times 3} & \mat{R}_{_{IE}} \end{bmatrix}
	\end{align}
\end{itemize}

\subsection{Dynamics}
\label{sec:dynamics}
The dynamics of a rigid body moving in space can be described by a set of equations of motion (EoM), which relate the forces acting on the body to its acceleration. The EoM can be expressed in the inertial earth-fixed frame $I$ or a body-fixed frame $E$ (in fact, any other frame could be chosen as well). However, when it comes to computational efficiency, it is usually best to choose a body-fixed frame, since otherwise the mass matrix $\mat{M}$ and the vector of centrifugal and Coriolis forces $\vec{b}$ become orientation dependent. On the other hand, the effect of forces which are constant in the inertial frame, such as gravity and buoyancy, will then be orientation dependent. In order to further simplify the equations, the origin of frame $E$ was chosen to lie at the geometrical centre of the main box (for an illustration see Section \ref{sec:conventions}).\\
The external effects acting on \textsc{Scubo} that were modelled are gravity, buoyancy, damping and added mass/inertia. The last effect is due to the fact that an accelerating body has to displace and accelerate the surrounding fluid as well.\\
These choices lead to the structure of the EoM given by the equation:\\
\begin{equation}\label{eq:eom}
	\left( {\mat{M}} + {\mat{M}_{_{A}}} \right) \cdot _{_E}\dot{\vec{\theta}}  + \vec{b} \left( _{_E}\vec{\theta} \right) + \vec{b}_{_A} \left( _{_E}\vec{\theta} \right) + \vec{D} \left( _{_E}\vec{\theta} \right) + \vec{g} \left( \vec{\varphi} \right) = _{_E}\vec{\tau}_{_{\rm{Thusters}}} + _{_E}\vec{\tau}_{_{\rm{Disturbances}}}
\end{equation}
The following list gives a detailed explanation of the individual terms and variables.
\begin{itemize}
	\item $\mat{M}$: Mass matrix. Given the mass of \textsc{Scubo} is $m$ and its moment of inertia w.r.t. the centre of mass $C$ expressed in frame $E$ is $\mat{_{_E}\Theta_{_C}}$, one obtains:
	\begin{align}
		\mat{M}=_{_{EE}}\mat{J}_{_{P,CE}}^T \cdot {m} \cdot {}_{_{EE}}\mat{J}_{_{P,CE}}+{}_{_{EE}}\mat{J}_{_{R,CE}}^T \cdot _{_E}\Theta_{_C} \cdot {}_{_{EE}}\mat{J}_{_{R,CE}}.
	\end{align}
	\item $\vec{b} \left( _{_E}\vec{\theta} \right)$: Vector of centrifugal and Coriolis forces. Computation:
	\begin{align*}
		\vec{b} \left( _{_E}\vec{\theta} \right) = _{_{EE}}\mat{J}_{_{P,CE}}^T \cdot {m} \cdot {}_{_{EE}}\dot{\mat{J}}_{_{P,CE}} \cdot {}_{_E}\vec{\theta} + {}_{_{EE}}\mat{J}_{_{R,CE}}^T \cdot {}_{_E}{\Theta _{_C}} \cdot {}_{_{EE}}\dot{\mat{J}}_{_{R,CE}} \cdot {}_{_E}\vec{\theta} + \dots
	\end{align*} \vspace{-20pt}
	\begin{align}
		\dots {}_{_{EE}}\mat{J}_{_{R,CE}}^T \cdot \left( {}_{_{EE}}{\mat{J}_{_{R,CE}}} \cdot {}_{_E}\vec{\theta} \right) \times \left( {}_{_E}{\Theta _{_C}} \cdot {}_{_{EE}}{\mat{J}_{_{R,CE}}} \cdot {}_{_E}\vec{\theta} \right)
	\end{align}
	\item $\vec{g} \left( \vec{\varphi} \right)$: Vector of potential forces, i.e., gravity and buoyancy. Given the acceleration due to gravity $g$, the
	Volume $V$ of \textsc{Scubo}, the density $\rho$ of water and the location $_E\vec{r}_{EB}$ of the centre of buoyancy, one obtains:
	\begin{align}
		\vec{g} \left( \vec{\varphi} \right) = {}_{_{IE}}\mat{J}_{_{P,CE}}^T \cdot \begin{bmatrix}	0 \\ 0 \\ +mg \end{bmatrix} + \begin{bmatrix} \mat{R}_{_{IE}} & -\mat{R}_{_{IE}} \cdot {}_{_E}\vec{r}_{_{EB}}^{\times} \end{bmatrix}^T \cdot \begin{bmatrix}	0 \\ 0 \\ - V\rho g \end{bmatrix}
	\end{align}
	\item $_{_E}\vec{\tau}_{_{\rm{Thrusters}}}$: Vector containing the forces and torques exerted by the thrusters. The eight thrusters $k=1,2,\dots,8$ have position $_{_E}\vec{r}_{_{\rm{Thruster},k}}$ w.r.t. the frame $E$ and exert an oriented force $_{_E}\vec{F}_{_{\rm{Thruster},k}}$ . This yields the generalized forces:
	\begin{align}
		_{_E}\vec{\tau}_{_{\rm{Thrusters}}} = \sum\limits_{k = 1}^8 \begin{bmatrix} \mat{I}_{3 \times 3} & - {}_{_E}\vec r_{_{\rm{Thruster},k}}^ \times \\ 0_{3 \times 3} & \mat{I}_{3 \times 3} \end{bmatrix}^T \cdot \begin{bmatrix} _{_E}{\vec F}_{_{\rm{Thruster},k}} \\ 0 \end{bmatrix}
	\end{align}
	\item $_{_E}\vec{\tau}_{_{\rm{Disturbances}}}$: Vector representing forces and torques created by disturbances such as currents or waves.
	\item $\mat{M}_{_A}$ and $\vec{b}_{_A}(_{_E}\vec{\theta})$: Mass matrix and vector of centrifugal and Coriolis forces related to added mass. Their calculation is similar to the one of $\mat{M}$ and $\vec{b}(_{_E}\vec{\theta})$. However, now three directional mass terms, $m_{_Ax}$, $m_{_Ay}$, $m_{_Az}$ and the moment of inertia tensor $diag\left(I_{_{A,xx}}, I_{_{A,yy}}, I_{_{A,zz}}\right)$ have to be considered. This diagonal structure implies that cross-coupling is neglected, but simplifies the parameter identification, since only six coefficients need to be determined.\\ With $u$, $v$, $w$ denoting the linear velocities and $r$, $p$, $q$ the angular rates expressed in frame $E$, one then obtains:
	\begin{align}
		\mat{M}_{_A} = diag\left( {m_{A,x}},{m_{A,y}},{m_{A,z}},{I_{A,xx}},{I_{A,yy}},{I_{A,zz}} \right)
	\end{align}
	\begin{equation}
		\vec{b}_{_A}(_{_E}\vec{\theta}) = \begin{bmatrix}
		m_{A,x} \cdot \left( {qw - rv} \right) \\ 
		m_{A,y} \cdot \left( {ru - pw} \right) \\ 
		m_{A,z} \cdot \left( {pv - qu} \right) \\ 
		qr \cdot \left( I_{_{A,zz}} - I_{_{A,yy}} \right) \\ 
		rp \cdot \left( I_{_{A,xx}} - I_{_{A,zz}} \right) \\ 
		pq \cdot \left( I_{_{A,yy}} - I_{_{A,xx}} \right)
		\end{bmatrix}
	\end{equation}
	\item $\vec{D}(_{_E}\vec{\theta})$: Vector of damping forces. Since the calculation of the friction forces is rather difficult	(dependence on Reynolds number, flow conditions, surface geometry, etc.) the assumption of pure velocity dependency was made. Cross-coupling, which for instance occurs when driving curves, is completely neglected and furthermore, only first and second order terms are considered. These simplifications, as proposed by Antonelli (2014, \cite{AntonelliAntonelli2014}, p. 39) and Fossen (1994, \cite{Fossen1994}, p. 43), lead to the structure shown below, which still requires the identification of 12 coefficients (denoted by $C$).\\
	\begin{equation}
		\vec{D}(_E\vec{\theta}) = \begin{bmatrix}
		C_{u1}u + C_{u2} \left| u \right|u \\ 
		C_{v1}v + C_{v2} \left| v \right|v \\ 
		C_{w1}w + C_{w2} \left| w \right|w \\ 
		C_{p1}p + C_{p2} \left| p \right|p \\ 
		C_{q1}q + C_{q2} \left| q \right|q \\ 
		C_{r1}r + C_{r2} \left| r \right|r
		\end{bmatrix}
	\end{equation}
	
\end{itemize}


\subsection{Simulink Model}
\label{sec:sim_model}
The EoM were implemented in \textsc{Simulink} as shown in Figure \ref{fig:sim_model}. The inputs are the thruster forces and the outputs the generalized coordinates $\vec{\varphi}$, the generalized velocities $_{_E}\vec{\theta}$ and the derivatives $_{_E}\dot{\vec{\theta}}$.\\
Using the \textquotedblleft Virtual Reality Modelling Language\textquotedblright{} (VRML) a world was created in \textsc{Simulink} in order to visualize the outputs of the simulation. An image of this world can be seen in Figure \ref{fig:vrml_world}.
\begin{figure}[h]
	\centering
	\includegraphics[width=\columnwidth]{images/yvain/sim_model.pdf}
	\caption{\textsc{Simulink} Model of \textsc{Scubo}}
	\label{fig:sim_model}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\columnwidth]{images/yvain/vrml_world.png}
	\caption{VRML World}
	\label{fig:vrml_world}
\end{figure}


\section{Modelling Parameters}
\label{sec:modelling_parameters}
This section gives a list of the parameters used in the \textsc{Simulink} model. The damping coefficients were determined from experimental data (see Appendix \ref{data:swimmingpool}) using linear least squares. However, one should note that due to the small number of measurements, the resulting coefficients are afflicted with small errors.\\
The coefficients related to added mass can be calculated using potential flow theory. The ellipsoid, being one of the only bodies for which analytical solutions exist, was chosen to estimate these coefficients. The exact formulas can be found in (Korotkin, 2008, \cite{Korotkin2008}, p. 81). The lengths of the half-axes were taken as 0.6m, 0.4m and 0.3m.\\
As the inertia tensor is rather difficult to determine, \textsc{Scubo} was approximated as a solid cuboid with constant mass distribution and dimensions 0.6m, 0.4m and 0.3 m.\\
The complete list of the numerical parameter values is:
\begin{itemize}
\item Gravity: $g = 9.81$ m/s$^2$
\item Mass: $m = 25$ kg
\item Inertia: $\mat{_{_E}\Theta_{_C}} = \text{diag}\left( {0.48,0.86,1.0} \right)$ kg m$^2$
\item Buoyancy (\textsc{Scubo} has a net uplift of 0.5 kg): $V\cdot\rho\cdot g = (m + 0.5$ kg) $\cdot$ $g$
\item Added mass: $\mat{M}_{_A} = \text{diag}( 24$ kg$, 48$ kg$, 83$ kg$, 1.0$ kg m$^2, 7.7$ kg m$^2, 2.5$ kg m$^2$)
\item \begin{minipage}[t]{0.3\columnwidth}
	Damping coefficients:\\
\end{minipage}
\begin{minipage}[t]{0.3\columnwidth}
	$C_{u1} = 173$ kg/s\\ 
	$C_{v1} = 326$ kg/s\\ 
	$C_{w1} = 139$ kg/s\\ 
	$C_{p1} = 1.05$ kg m$^2$/s\\ 
	$C_{q1} = 1.67$ kg m$^2$/s\\ 
	$C_{r1} = 5.49$ kg m$^2$/s
\end{minipage}
\begin{minipage}[t]{0.5\columnwidth}
	$C_{u2} = -54$ kg/m \\ 
	$C_{v2} = -180$ kg/m\\ 
	$C_{w2} = -77$ kg/m\\ 
	$C_{p2} = 0.0521$ kg m$^2$\\ 
	$C_{q2} = 1.10$ kg m$^2$\\ 
	$C_{r2} = 2.40$ kg m$^2$
\end{minipage}
\item Centre of buoyancy (assumed to lie at the geometrical centre, since \textsc{Scubo} is nearly symmetric about its main axes):
 $_E\vec{r}_{EB} =  \begin{bmatrix}0&0&0\end{bmatrix}^T$ m
\item CoM (estimation based on experiments with the attitude controller described in Section \ref{sec:attitude_controller}): $_E\vec{r}_{EC} =  \begin{bmatrix}0&0&-0.015\end{bmatrix}^T$ m
\item Position of the first thruster (due to symmetry, the position of the other seven thrusters is obtained by changing signs): $_E\vec{r}_{ET,1} =  \begin{bmatrix}0.265&0.14&0.08\end{bmatrix}^T$ m

\end{itemize}



\section{Control}
\label{sec:control}
In order to be able to navigate through sensitive environments, take images from any angle but also to facilitate manual steering, \textsc{Scubo} is equipped with a position and an attitude controller. These were first tested and tuned in \textsc{Matlab}/\textsc{Simulink} and then implemented in ROS/C++. In addition, the allocation, which consists of the mapping of the six forces/torques expressed in the body-fixed frame to the eight thruster forces, is explained. For further details, particularly on the controllers, the Bachelor thesis by Yvain de Viragh should be considered.


\subsection{Control Structure}
\label{sec:control_structure}
The entire control structure was implemented in ROS/C++ as seen in Figure \ref{fig:control_structure} and runs with a frequency of 100 Hz. The \verb+processor+ node receives data from an IMU, ROVIO, a pressure sensor, a spacemouse and, if desired, from a node publishing trajectory commands in form of the desired state of the system. Using these signals, the \verb+processor+ node publishes the actual and the desired state of the system (position, orientation, linear velocities and angular rates), as well as a force/torque commands based on the spacemouse input.\\
With the information on the state, the controllers compute the necessary forces and torques, which -- together with the spacemouse commands -- are mapped to PWM signals by the \verb+allocation+ node.\\
Inside the \verb+pressure+ node, the calculation of the depth and the velocity in $z$-direction happen, which implies taking a derivative. Since the raw pressure data is affected by noise and outliers, a 10th order median filter and a first order lowpass filter with a decay time of 0.1 seconds are applied to smooth it out sufficiently.\\
For safety reasons, the spacemouse and the controllers can be turned off simply by pressing the buttons on the left and right side of the spacemouse. If the spacemouse is turned off, all six entries in the message \verb+E_u_T_Spacemouse+ are set to zero. If the controllers are turned off, the \verb+processor+ node sets the boolean message \verb+controller_on+ to \verb+false+ which will cause the controllers to set their force/torque commands to zero.\\
Furthermore, a trajectory will only be followed if the spacemouse is turned off, and for safety reasons it will immediately be interrupted, when the spacemouse is turned on again by pressing its left button.\\
Another important task performed by the \verb+processor+ node is to detect whether ROVIO is running and publishing information on the position of the system or not. If no messages are received, the \verb+processor+ node automatically sets the $x$ and $y$ entries of the current and desired position and velocity to zero and uses the data from the pressure sensor for the depth.\\
Concerning the user-friendliness, a key feature is the use of a \textquotedblleft dominant mode\textquotedblright. From the three translational and three rotational commands received from the spacemouse, the \verb+processor+ node will only transmit the one with the largest magnitude and set the others to zero. Our experience is that this facilitates the manual operation of \textsc{Scubo} for untrained users.\\
The user is also assisted by the controllers. When giving translational commands, the attitude is stabilized and when performing rotations, the position is kept. If the user gives no inputs, both attitude and position are maintained. Thanks to this setup, we felt a significant improvement in the controllability and stability of \textsc{Scubo}: With the controllers it is easily possible to drive straight lines and to maintain the desired orientation.
%The user is also assisted by the controllers. If he performs a translational movement, the desired position is set equal to the actual one, while the desired orientation is set equal to the one measured when he performed a rotational movement for the last time. The opposite happens when he performs a rotational movement: The desired orientation is set equal to the current orientation and the desired position is set equal to the one measured when he performed the last translational movement.\footnote{It should be noted that setting the desired position equal to the last measured value is done with a delay of one second, during which \textsc{Scubo} is slowed down by the large dissipative forces. Otherwise the user might see \textsc{Scubo} driving in the opposite direction for a certain distance, since instant stabilization is not possible due to the kinetic energy stored in the system.} If the user gives no input, the desired state is simply set equal to then one reached when the last nonzero command was given. During our tests we felt a significant improvement in the controllability and stability of \textsc{Scubo}: With the controllers it is easily possible to drive straight lines and to maintain the desired orientation.
\begin{figure}[h]
	\centering
	\includegraphics[width=\columnwidth]{images/yvain/rosgraph.pdf}
	\caption{ROS Computation Graph of the Control Structure}
	\label{fig:control_structure}
\end{figure}


\subsection{Attitude Controller}
\label{sec:attitude_controller}
The attitude controller is based on the control law proposed in the paper \textquotedblleft Geometric Tracking Control of a Quadrotor UAV on SE(3)\textquotedblright{} (Lee, 2010, \cite{LeeLeokyMcClamroch2010}). To compensate for moments induced by an offset between the CoM and the centre of buoyancy, it is improved with an adaptive model-based term as proposed by Antonelli (2001, \cite{AntonelliChiaveriniSarkarEtAl2001}). This term is adaptive in the sense that an estimation of the three dimensional offset $\vec\pi$ (difference between the CoM and the centre of buoyancy) is made by the controller.
The control law is given as follows:
\begin{equation}
_E\vec{u}_{T} = \left[ {\begin{array}{*{20}{c}}
{{0_{3 \times 1}}}\\
{ - {K_P} \cdot {{\vec e}_R} - {K_D } \cdot {{\vec e}_D } - {K_I} \cdot \int {{{\vec e}_R}dt}  - {K_T} \cdot \Phi \left( {{R_{IE,\;des}}} \right) \cdot \vec \pi }
\end{array}} \right]
\end{equation}
where
\begin{equation}
\vec{e}_R = \frac{1}{2}{\left( {R_{IE,\;des}^T \cdot {R_{IE}} - R_{IE}^T \cdot R_{IE,\;des}^T} \right)^ \vee }
\end{equation}
\begin{equation}
\vec{e}_D = {}_E{\vec \omega _{IE}} - R_{IE}^T \cdot {R_{IE,\;des}} \cdot {}_E{\vec \omega _{IE,\;des}}
\end{equation}
%\begin{equation}
%\vec{e}_I = \left\{ {\begin{array}{*{20}{l}}
%{{{\vec e}_R}}&{{\rm{if}}\;\;\left\| {{{\vec e}_R}} \right\| > 0}\\
%0&{{\rm{else}}}
%\end{array}} \right.
%\end{equation}
\begin{equation}
\dot{\vec{\pi}}  = \left\{ {\begin{array}{*{20}{l}}
{{K_M} \cdot {\Phi ^T}\left( {{R_{IE,\;des}}} \right) \cdot {{\vec e}_R}}&{{\rm{if}}\;\left\| {{{\vec e}_R}} \right\| < \epsilon_R}\\
0&{{\rm{else}}}
\end{array}} \right.
\end{equation}
\begin{equation}
\Phi \left( {{R_{IE,\;des}}} \right) = mg \cdot \left( {\begin{array}{*{20}{c}}
0&{{R_{IE,\;des,\;33}}}&{ - {R_{IE,\;des,\;32}}}\\
{ - {R_{IE,\;des,\;33}}}&0&{{R_{IE,\;des,\;31}}}\\
{{R_{IE,\;des,\;32}}}&{ - {R_{IE,\;des,\;31}}}&0
\end{array}} \right)
\end{equation}
With the help of the \textsc{Simulink} model, the following gains were iteratively determined:
\begin{equation}
\begin{array}{lcl}
{K_P} &=& {\mathop{\rm diag}\nolimits} \left( {30,40,50} \right)\\
{K_D} &=& {\mathop{\rm diag}\nolimits} \left( {25,50,25} \right)\\
{K_I} &=& {\mathop{\rm diag}\nolimits} \left( {25,25,25} \right)\\
{K_T} &=& {\mathop{\rm diag}\nolimits} \left( {{\rm{7.22,2.34,3.46}}} \right)\\
{K_M} &=& 0.0001\\
{\epsilon_R} &=& 0.3
\end{array}
\end{equation}
In order to prevent reset windup, the I-part of the controller is saturated, if it reaches a norm larger than 2.

\subsection{Position Controller}
\label{sec:position_controller}
The position controller implemented on \textsc{Scubo} is shown as \textsc{Simulink} model in Figure \ref{fig:position_controller}. It is a PID controller expressed in the inertial frame $I$, in order to optimally compensate for currents. By multiplying the resulting signal with the transpose of $\mat{R}_{_{IE}}$, the corresponding forces expressed in the body-fixed frame $E$ are obtained. To prevent reset-windup, the input to the integrator is set to zero for values with a magnitude larger than 0.1 m. Furthermore, high output values generated by the differentiation due to rapid changes in the setpoint are avoided by only feeding the current position to the D-part. The implementation in ROS/C++ differs insofar as the velocity is used, instead of the derivative of the position.
\begin{figure}[h]
	\centering
	\includegraphics[width=\columnwidth]{images/yvain/position_controller.pdf}
	\caption{\textsc{Simulink} Model of the Position Controller}
	\label{fig:position_controller}
\end{figure}


\subsection{Allocation}
\label{sec:allocation}
The core of the \verb+allocation+ node is the function shown in Code \ref{code:allocation_function}. Its input is a vector with six entries, containing the desired force and torque commands expressed in frame $E$. The output is a vector with eight entries, which represent the forces which should be exerted by the thrusters. The mapping is based on the principle of superposition: Each of the main three forces and three torques ($x$,$y$,$z$ direction in frame $E$) can be produced by four thrusters and by superposition, every arbitrary force or torque can be realized (obviously there is a limitation in the magnitude due to saturation). Also, this function makes sure that the thruster forces are positive, meaning that they are always turning in \textquotedblleft forward mode\textquotedblright{} and not in \textquotedblleft reverse mode\textquotedblright, since the latter has a lower efficiency.\\
In order to account for the limitations of the thrusters, the forces are saturated such that the relative magnitudes are conserved.\\
Finally, the PWM signals are calculated, where a linear dependency between force an PWM signal is assumed.\\
\newpage
\begin{lstlisting}[caption=Allocation Function, label=code:allocation_function]
Eigen::VectorXd E_u_T2F_T(const Eigen::VectorXd& E_u_T)
{
    Eigen::MatrixXd m1(8,6);
    Eigen::MatrixXd m2(8,6);

    m1 << 0,  0,  0,  0,  1,  0,
          1,  0,  0,  0,  0,  1,
          1,  1,  0,  1,  0,  0,
          0,  1,  0,  1,  1,  1,
          0,  0,  1,  1,  0,  0,
          1,  0,  1,  1,  1,  1,
          1,  1,  1,  0,  1,  0,
          0,  1,  1,  0,  0,  1;

    m2 = Eigen::MatrixXd::Constant(8,6,1) - m1;

    Eigen::VectorXd F_T(8);
    F_T = Eigen::VectorXd::Zero(8);

    for(int i=0; i<6; i++)
    {
        if(E_u_T(i)>0) F_T += m1.col(i)*E_u_T(i);
        else if(E_u_T(i)<0) F_T -= m2.col(i)*E_u_T(i);
    }

    return F_T;
}
\end{lstlisting}