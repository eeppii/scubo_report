\chapter{Mechanical Design}
\label{sec:mechanical}
The following chapter covers the mechanical implementation of the concept. This includes detailed design decisions as well as their realization. The definition of the dimension and of the design material are addressed as well. \\
\\
Please note that an introduction about how to properly set up the hardware of \textsc{Scubo} is found in Appendix \ref{data:sealingInstruction} and Appendix \ref{data:startupInstruction}. If you want to operate \textsc{Scubo}, please get familiar with the warning sheet in Appendix \ref{data:warnings} as well.\\


\vspace{1cm}

\minitoc
\newpage


\section{Overview}
\label{sec:overview}

\textsc{Scubo} consists of a central unit in the form of a cuboid in which the electronics are stored. A duct goes through this unit to improve the fluid-dynamics and the cooling of the inner electronics.
The eight thrusters are attached to the central unit by the so called thruster arms, which also provide a way possibility to adjust the total weight and the CoM. Further, on each side except the top, a modular port is attached to the robot. To these ports, additional sensors, cameras or lights can be attached. For each of the six directions, a steering camera is provided: a high resolution industrial camera in the front and webcams in the other directions.\\

\section{Central Unit}
\label{sec:central_unit}

All of the electronic components including the four batteries are stored in the central unit. This unit consists of a box made of carbon fibre (29.0 $\times$ 32.8 $\times$ 23.8 cm L$\times$W$\times$H, approximately 4 kg) with two open walls and a duct (cross sectional area 15.0 x 10.0 cm) through the middle along the $x$-axis. Two aluminium hatches close the central unit as seen in Figure \ref{pics:central_unit}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=8cm]{images/leonie/central_unit_coordinate_system.png}
	\caption{Central Unit: Carbon Box with Hatches}
	\label{pics:central_unit}
\end{figure}

\subsection{Dimensions}
\label{sec:main_box_dimensions}
The box needs to be as small as possible, since the weight of the water displaced by the robot is equal to the one needed to achieve neutral buoyancy\footnote{A more detailed description regarding the buoyancy calculations can be found in Section \ref{sec:weight}.}. To achieve maximal space efficiency, the central unit and the duct need to be cuboids, instead of an ellipsoid with a circular duct. This allows for an optimal arrangement of the electronics. An additional outer shell can be attached to achieve the shape of an ellipsoid or any other desired shape (see Section \ref{sec:shell}).
The ideal size for the duct was evaluated in a test case (see full test protocol in Appendix  \ref{data:t200}). We determined the size of the box with styrofoam models of the electronic components assembled around the duct in different arrangements (seen in Figure \ref{fig:arrangements}). With a cable prototype (see Figure \ref{fig:final_arrangement}) the dimensions were finalized. 
\begin{figure}[h]
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[height=4cm]{images/leonie/Anordnung1.pdf}
	\end{minipage} \hfill
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[height=4cm]{images/leonie/Anordnung2.pdf}
	\end{minipage}
	\caption{Arrangements of Electronics inside \textsc{Scubo}}
	\label{fig:arrangements}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=6cm]{images/leonie/kabelprototyp.pdf}
	\caption{Final Cable Prototype}
	\label{fig:final_arrangement}
\end{figure}

\subsection{Material}
\label{sec:main_box_material}

For the central unit, carbon fibre and aluminium were under consideration. Steel was no option since with its use, a maximal weight of 30 kg would have been impossible. We opted for carbon fibre because of its smaller density (1.58 g/cm$^3$) compared to aluminium (2.70 g/cm$^3$) while sustaining the same forces\footnote{The density of the carbon composite is an approximation because it depends on the fibre and epoxy resins used.}. Carbon composite can be laminated in the desired form whereas aluminium and steel plates need to be welded together to form a box. Welded aluminium bears the high risk that the seam does not sustain the pressure difference and makes the box leak.\\
\\
Furthermore, FEM simulations showed that the curvatures of the edges in addition with four support beams are sufficient to provide a stable box to a pressure difference of at least 3 bar. The manufacturing of such curvatures in aluminium would have beard high costs and in addition, supports and cross beams in the material would have been required. \\
In Appendix \ref{data:analysis_unit}, the full analysis of the carbon fibre central unit can be found.\\
%Furthermore carbon fibre (wall thickness 5 mm) sustains the 2 bar pressure difference between the surroundings and the insides without additional cross beams in the material. FEM simulations showed that the curvatures of the edges are sufficient to provide a stable box. In Appendix \ref{data:analysis_unit} a full analysis of the carbon fibre central unit can be found.\\
\\
%The cooling of the electronics results from the surrounding water. The smaller heat exchange using carbon composit compared to aluminium is still sufficient because of the big outer surface of the central unit with the duct.\\
The material of the hatches is aluminium and not carbon fibre due to the complex design. The manufacturing of an aluminium milling component is of lower cost and time duration. Especially the notch for the O-ring (see Section \ref{sec:sealing}) would have beard problems with the use of carbon fibre.
\subsubsection{Corrosion}
To protect the aluminium parts from corrosion in salt or chlorine water, the company \textsc{Alumex AG} anodised all of the aluminium components. We did not consider any other approach because the projects \textsc{Sepios} and \textsc{Nanins} successfully used anodic treatment to protect their shells as well.\\ \\
Because the central unit and the hatches are of different materials, galvanic corrosion can become a problem. The underwater environment quickens the process additionally. However, with the anodic treatment of the aluminium parts and the thick resin layer on the carbon, there were no problems expected within the time duration of this project\footnote{Based on information from Patrick Meyer (\textsc{Carbomill AG}) as well as Thomas Heinrich (ETH Z\"urich \textquotedblleft Laboratory of Composite Materials and Adaptive Structures\textquotedblright).} and therefore this was neglected. If the project is further pursued, the problem should be taken into account. A possible solution is to hard coat the aluminium or to use glass fibre for the outer layer of the carbon fibre box.
\subsection{Hatches} \label{sec:hatches}
To maintain the desired accessibility, a simple fastening mechanism is required.
%As recap, the goal of \textsc{Scubo} is among others to be a low maintenance robot and easy customizable for new challenges. A partial solution to achieve this, is that the electronics have to be reached fast and without big tooling equipment.
Two solutions were found. The first is to close the hatches with several screws, the other is to use quick release fasteners. For sealing reasons, each of the hatches has to be pressed onto the box with a force of around 25'000 N (see Appendix \ref{data:weight_balance}). The available space on the box allows to mount 6 fasteners per hatch, therefore each has to apply a force of 4'000 N. An evaluation of the market (see Appendix \ref{data:clamps}) led us to a single fastener, which conforms to the given restrictions in weight and dimension. Fasteners have the advantage that they are quicker to open than screws. However, these fasteners are able to exert a force of at least 6'200 N, therefore an unintentional overtightening could have damaged the carbon. Furthermore, clamps are fifteen times heavier than screws. This is why we decided to use 10 screws per side to fasten the hatches.
%\subsubsection{Clamps}
%In the concept the mounting of the flaps is by quick release fasteners. Based on the sealing, one fastener have to apply a force around 4000 Newton each. \textbf{Verweis auf Anhang mit Rechnung)}. An evaluation of the market lead us to only a few different fastener. Due to our restriction in weight and dimension, only one clamp fulfils our needs. This clamp is a special clamp, called rot-lock.\textbf{Bild hinzufügen und satz besser formulieren.}
%\subsubsection{Screws}
%Finally the flaps are mounted by screws. The motive for doing this are on one side the reduction of  risks \textbf{Verweis FMEA} and on the other the possibility to reduce weight. The risk was mainly that the conjunction of the clamp and the carbon is not able to withstand the forces applied in this \footnote{There are some other reasons why not using clamps, for a detailed risk analysis refer to \textbf{Verweis appendix}}. Because a failure of the box is not affordable for us neither financially nor with the time management, we decided to use screws instead of clamps.
\subsection{Supports} \label{sec:support}
To ensure the durability, eight aluminium supports were inserted between the duct and the outer walls of the carbon box. The supports have a truss structure in order to be light weighted and strong at the same time. Using FEM simulation, the supports were optimised until the box would sustain a pressure difference of 3 bar.

\section{Exterior Parts}
In this section the parts outside of the main box are introduced. 
\subsection{Modular Ports}
\label{sec:modularports}
One feature of \textsc{Scubo} is the ability to add and connect any type of sensor from the outside to the robot. This modularity is achieved through a special design that allows a secure mounting of those electronic gadgets.\\

% As an example a mountable UV lamp is shown in Appendix \ref{data:uv_lamp}.\\

Docking stations with a connection to the inside of the box are provided on three places (left, right and bottom side). At these places electronic tools can be plugged to the board computer with the help of watertight USB3.0 connectors, as shown in Figure \ref{fig:usb}.\\


Further, sensors that are not in need of a data connection, can be attached to the robot by appointed fixture points at the front and rear bumper.\footnote{As an example a mountable UV lamp is described in Appendix \ref{data:uv_lamp}.}
In case, any of these gadgets need electricity, \textsc{Scubo} is equipped with two power plugs on its top side (see Figure \ref{fig:bumperpower}).\\
\\
%This allows a secure application of the modularity without endangering the electronics inside the box (as shown in Figure \ref{fig:usbcable}).

%Further modularity is provided with the six cameras attached to \textsc{Scubo}. These modular camera housings consist of three pieces. Firstly, an aluminium ring which is watertightly glued to the carbon box in combination with a cable gland secures the opening to the box. Secondly, there is a bridge which maintains a certain distance from the box to the camera. This bridge has an O-ring on both sides and can be manufactured at other lengths if needed. Finally, as shown in Figure \ref{fig:modports}, a sensor housing is mounted on this bridge which completes the sealing. This design enables the user to change each part independently of the central unit. If someone wants to replace the cameras with bigger ones, only the sensor housing needs to be replaced.\\ 
 %It can be seen that, if water enters the box or the bridge due to bad mounting or material failure, the watertight cable gland establishes a border to the box and secure the electronics inside.

%\begin{figure}[h]
%	\begin{minipage}[t]{0.5\columnwidth}
%		\centering
%		\includegraphics[height=3.6cm]{images/ilyas/usb.pdf}
%	\end{minipage}
%	\hfill
%	\begin{minipage}[t]{0.5\columnwidth}
%		\centering
%		\includegraphics[height=3.6cm]{images/ilyas/cable_gland.png}
%	\end{minipage}
%	\caption{Watertight USB3.0 and Cable Gland}
%	\label{fig:usbcable}
%\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=7cm]{images/ilyas/usb.pdf}
	\caption{Watertight USB3.0 Connector}
	\label{fig:usb}
\end{figure}

\begin{figure}[h]
	\begin{minipage}[t]{0.4\columnwidth}
		\centering
		\includegraphics[height=4.5cm]{images/ilyas/bumper.pdf}
	\end{minipage}
	\hfill
	\begin{minipage}[t]{0.6\columnwidth}
		\centering
		\includegraphics[height=4.5cm]{images/ilyas/powerplugs.jpg}
	\end{minipage}
	\caption{Docking Points with additional Power Plugs}
	\label{fig:bumperpower}
\end{figure}


Further modularity is provided with the four cameras attached to \textsc{Scubo}'s central unit. These modular ports consist of three pieces. Firstly, an aluminium ring which is watertightly glued to the carbon box in combination with a cable gland secures the opening to the box. Secondly, there is a bridge, which maintains a certain distance from the box to the sensor housing. This bridge has an O-ring on both sides and can be manufactured at other lengths if needed. Finally, as shown in Figure \ref{fig:modports}, a sensor housing with an acrylic glas is mounted onto this bridge. This design enables to change each part independently of the central unit. However, if one is interested in replacing the cameras with bigger ones, only the sensor housing needs to be replaced.\\ 
%It can be seen that, if water enters the box or the bridge due to bad mounting or material failure, the watertight cable gland establishes a border to the box and secure the electronics inside.

\begin{figure}[h]
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[height=4.5cm]{images/ilyas/housing.pdf}
	\end{minipage}
	\hfill
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[height=4.5cm]{images/ilyas/modularport_section.PNG}
	\end{minipage}
	\caption{Section of a Modular Port with a mounted Camera Case}
	\label{fig:modports}
\end{figure}
%At 3 places of the robot, watertight USB 3.0 are provided to connect sensors to the board computer. 
\subsection{Thruster Arms} \label{sec:thrusterarms}
The eight thrusters are mounted to the main box via aluminium arms. Screws and nuts can be inserted in order to adjust the weight of the robot and therefore adapt the uplift (see Figure \ref{pics:thruster_arm}). Furthermore, this allows us to change the location of the CoM. We opted for this solution because of its simplicity. Screws and nuts are available everywhere, inexpensive, and they allow to regulate the additional weight by steps of two grams without having to open the robot. In total, a weight of 2.3 kg can be added (for more details see Section \ref{sec:weight}).
\begin{figure}[h]
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[height = 4cm]{images/retob/thruster-1.png}
	\end{minipage}
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[height = 4cm]{images/retob/thruster-2.jpg}
	\end{minipage}
	\caption{Thruster Arm: Screws to adjust Weight}
	\label{pics:thruster_arm}
\end{figure}
\subsection{VI-Sensor Housing} \label{sec:visensor}
As described in Section \ref{subsec:locandcoll}, we decided to use a VI-Sensor for localisation. The position on the bottom of the robot (see Figure \ref{pics:VI-Sensor}) has different reasons. The first one is that for localisation, we need detectable features. That is why the field of view is directed towards the sea floor. An operation upside down would be possible, but should not be the normal operation condition. Another requirement for satisfying results is sufficient lighting. Therefore, the stereo cam (positioned in the middle of the housing) is oriented to the front where the lights can be used for dark environments (see Section \ref{sec:lights}). With the sensor on the bottom of the robot, the effects of mirroring on the water surface are minimized as well. It also encloses the photo diode (to the left of the stereo cam) mentioned in Section \ref{subsec:ssb}.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\columnwidth]{images/thomas/vi.jpg}
	\caption{VI-Sensor Housing}
	\label{pics:VI-Sensor}
\end{figure}
	
\subsection{Front and Rear Bumper} \label{sec:bumpers}
Because of the water streams of the thrusters, the front and rear modular ports and cameras cannot be placed directly on the carbon box. Instead, two bumpers were designed, manufactured out of aluminium and mounted between the thruster arms (see Figure \ref{fig:bumpers}), such that they provide enough distance to get clear camera images. Inspired by the automotive industry, they can also absorb the impact in case of a collision.
\begin{figure}[h]
	\centering
	\includegraphics[width=7.3cm]{images/mneumann/bumper.jpeg}
	\caption{Front and Rear Bumper before Anodic Treatment}
	\label{fig:bumpers}
\end{figure}

The mechanical requirements that these bumpers have to fulfil are the following:
\begin{itemize}
	\item big surface between arm and box in order to uniformly distribute the force of possible collisions,
	\item provide a platform to mount one camera and one modular port per bumper outside the water stream of the thrusters.
\end{itemize}

\subsection{Lamp Support} \label{sec:lampsupport}
The lamps (see Section \ref{sec:lights}) are mounted with a 3D printed support, which is shown in Figure \ref{fig:lampenhalterung}. We chose this solution for various reasons: Before delivery, we did not know the exact dimensions of the lamps, and 3D printed supports enable a fast manufacturing. Furthermore, we designed the supports such that the orientation of the lamps can be adjusted. This means that the horizontal and vertical angle of the lamps with respect to \textsc{Scubo}'s main direction can be varied within $30^{\circ}$. This is needed as for different applications of \textsc{Scubo}, different light orientations are optimal. For taking pictures of objects located near to the front camera, the light cones of the lamps should intersect at the object's location. If the robot is needed to explore underwater environment without having a close look at underwater objects, the light cones of the lamps should intersect a few metres in front of \textsc{Scubo}, as described in Section \ref{sec:pix4d} and shown in Figure \ref{fig:floatpart}.

%Since we do not have enough data about the dimensions of the lamps, we chose a design that is fast in manufacturing and can be easily adapted.
%Our high intensity light we mount with a 3D printed support. This because we have not enough data too adapt the support to the lamp. We tried a lot to get this dimensions, but we don't get them. Therefore we chosen a construction that we can easily adapt and is fast in manufacturing. 
%Another reason for the 3D printed design is that for the cooling of the lamp a complex shape is necessary. Due to delivery problems we did not receive the right lamps, therefore the definite lamp support is not designed yet.

\begin{figure}[h]
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[width=0.9\columnwidth]{images/thomas/lampenhalterung1.jpg}
	\end{minipage}
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[width=0.7\columnwidth]{images/thomas/lampenhalterung2.png}
	\end{minipage}
	%	\caption{Side and top view of all angles of view}
	\caption[Lamp Support]{The Lamp Support}	
	\label{fig:lampenhalterung}
\end{figure}

\section{Outer Shell}
\label{sec:shell}
Several designs were considered, ranging from skeletal structures to organic freeforms (see Appendix \ref{data:shell}). The most promising were modelled in polyurethanfoam to scale. Finally, a geometrical corpus as the basis of the design was chosen, since the levels and edges contrast with the round and organic water world. This way \textsc{Scubo} strikes the eye while diving in the sea, but does not seem out of place.\\
The shell is made of single ABS plates that are partially thermoformed. This material was chosen due to its common use in outdoor applications. Bolted with internal angles made from aluminium, the plates form the outer shell (seen in Figure \ref{fig:shell}).

\begin{figure}[h]
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[width=0.9\columnwidth]{images/leonie/shell_total.jpg}
	\end{minipage}
	\begin{minipage}[t]{0.5\columnwidth}
		\centering
		\includegraphics[width=0.9\columnwidth]{images/leonie/shell_front.jpg}
	\end{minipage}
	\caption[Outer Shell]{Side and front View of Outer Shell}
	\label{fig:shell}
\end{figure}

%%%%%%ORIGINALTEXT INDIS%%%%%%%%%%
%Excited and highly motivated we started our journey of idea invention and soon we had worked out a wide range of solid concepts. From a geometrical skeletal structure to an organic freeform some designs were elaborated and by building some of the designs as models in polyurethanfoam to scale we were able to find concrete detail solutions fast, to perfect the shell design and to make the selection, which design would be the end result. The geometrical corpus as the basis of the \textsc{Scubo} design was chosen, because the levels and edges contrast starkly with the round, dynamic and organic water world. So \textsc{Scubo} makes itself stand out while diving in the sea, but nevertheless does not look out of place.\\
%The red inlay on both sides for the cameras represents a colour accent, which builds tension within the shell design and makes it attractive.\\
%The step further toward the realisation started with a material- and making selection. After consulting professionals the idea of making the modular system of \textsc{Scubo} out of single plates that are partially thermoformed stood up. As an appropriate material, because of its use in outdoor area we chose ABS. Bolted with internally angles made from aluminium the ABS forms a dynamic, cool outer shell for \textsc{Scubo}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Weight and Balance}
\label{sec:weight}

For simplicity, the robot has a net uplift. Therefore, \textsc{Scubo} reaches the surface automatically if the propulsion system fails. To achieve this, the robot needs to be lighter than the water it displaces. We assured this through a detailed volume and weight calculation found in Appendix \ref{data:volume} and \ref{data:weight}.\\
Because the robot is modular and can be equipped with all kinds of sensors, the weight and the CoM can vary. It can be adjusted as described in Section \ref{sec:thrusterarms}. \\
The batteries are placed nearly symmetrically inside the main box, as their heavy weight has the highest influence on the CoM of all interior parts. Furthermore, all exterior parts are arranged as symmetric as possible with respect to the main box in order to keep the centre of mass in the middle of the robot. \\ \\
The implementation of diving bells was also considered. We decided against them because they would have added unnecessary complexity to the system. Since the robot was intended to be very agile, the diving bells would only have made sense for buoyancy regulation but not for dynamic diving, since they have relatively slow dynamics. We determined that depth control is much faster using the thrusters (see detailed calculation in Appendix \ref{data:divingBellCalc}).

\section{Sealing}
\label{sec:sealing}
For each seal we use either a cable gland, an O-ring or a gasket, as described below. Furthermore, we use waterproofed plugs for the LAN and power plugs. The screws of the handle bars and the thruster arms are sealed with sealing washers. \\
The material of the O-rings and the gaskets is FPM \textsc{Viton}, because it sustains salt and chlorine water. \\
Tests in the autoclave showed that \textsc{Scubo} is air tight to a pressure difference of 2.5 bar (see Appendix \ref{data:autoclave1} and \ref{data:autoclave2}).
\subsubsection{Cable Glands} \label{sec:glands}
Whenever possible we use cable glands, since they are very durable, low priced and are standard parts. All of the cables (thrusters, cameras, lights and laser) are sealed with cable glands. The used ones are brass cable glands with a protection degree of either IP68 or IP69K.
\subsubsection{O-Rings} \label{sec:oring}
Where cable glands were no option, O-rings in a notch are used wherever possible (see Figure \ref{fig:oring}). We decided on O-rings because they are standard parts, low priced and, due to their small cross-sectional area, need a smaller contact pressure than gaskets.\
For the cameras and modular ports, radial seals are used. Using this method, the contact pressure is applied when the inner cylinder with the O-ring is inserted into the outer cylinder. The purpose of the screws is to prevent the part from moving. For most of the camera-glass seals and the hatches, normal O-ring seals are used. Here, the screws are applying the contact pressure.
\begin{figure}[h]
	\centering
	\includegraphics[width=7.5cm]{images/thomas/Camera_Housing.png}
	\caption{Radial and Normal O-Ring Sealing of Camera on Top}
	\label{fig:oring}
\end{figure}
\subsubsection{Gaskets} \label{sec:gaskets}
Gaskets are only used when there is no other option, because of the high contact pressure and the need to be custom made. \\
For the front and rear camera-glass seals (see Figure \ref{fig:gasket}) we use gaskets, because the bumpers were manufactured using a water-jet cutter and the use of an O-ring would have required additional milling.
\begin{figure}[h]
	\centering
	\includegraphics[height = 5cm]{images/leonie/sealing_gasket_vorne.png}
	\caption{O-Ring and Gasket Front Camera}
	\label{fig:gasket}
\end{figure}
%\section{Quality management<-ganzes Kapitel streichen? oder sehr kurz fassen!}
%\label{sec:quality}
%\subsection{Support}
%\label{sec:researchquality}
%\textbf{KURZ ERWÄHNEN VON ZENTRALWERKSTAT UND KUBO UND CARBOMILL UND DANKE SAGEN}
%To avoid trivial errors and to gain a feeling for the business, we talked to many different experts in this particular sector. Again we want to thank all of them for they great support. The most important and valuable experts are listed below.
%\begin{itemize}
%	\item \textbf{Zentralwerkstatt der Physik:} They helped us with a lot of standard engineering knowledge. Because we all do the first time real mechanical design, we had a lag of feeling for it. They helped us uncomplicated and with a great experience.
%	\item \textbf{KUBO} They are experts in sealing technology and shared this knowledge with us. With they help for the calculation we hope every sealing will fit the condition.
%	\item \textbf{Carbon} Because carbon and other composites are a huge topic, we needed a lot of expertise there. Aside our sponsor \textsc{Carbomill} we talked to many different other people to approve our concept \footnote{\textbf{Fussnote oder verweiss auf list, wo man alle listen kann}} in order to avoid any kind of fatal error, since this part of the concept leads to a great stop in our project.  
%\end{itemize}
%We have also done a lot of other interviews and clarifications. For a full list \textbf{see Appendix}. Beside that a lot of paper research was done.

%\subsection{Verification of Technical Drawings<-most likely raus}
%\label{sec:verificationdrawings}
%To avoid retardation in the project we have to pay attention that every part of the robot we give into production is perfect. To achieve that every part is perfect itself, we done different steps of "design freezes". In every of these freeze most of the design team join together and check every part if it's fully functional, fully producible and if it's possible to build the part into the robot, in the sense of assembling. In total we done four of these " design freezes". In every of them we create a list with deficits and the steps to perform to get the final release. At the last meeting, we were able to release all parts. After this date, all CAD-Model have been blocked, so that changes only can be made by creating an new revision and therefore changes can easily be traced.\\
%Due to the fact that the main supplier of our aluminium parts LIBS, need detailed technical drawings fo production, we also have to draw and check them. As well here an mistake in the drawings can stop our project for a long time. Therefore every drawing has been rechecked by another member of the design team and has been released by another one. A lot of missing dimension had been detected, so in the end only one unimportant dimension we missed and we get a phone for it. But nobody is perfect.
%\begin{figure}[h]
%	\begin{minipage}[t]{0.48\textwidth}
%		\includegraphics[width = \textwidth]{images/retob/freezing.jpg}
%	\end{minipage}
%	\hfill
%	\begin{minipage}[t]{0.48\textwidth}
%		\includegraphics[width = \textwidth]{images/retob/tech-drawin-list.jpg}
%	\end{minipage}
%	\caption{List to freeze the parts, control that every drawing is rechecked \textbf{da m\"ussen noch bessere Bilder rein}}
%	\label{pics:quality_management}
%\end{figure}

%KANN WOHL KOMPLETT GEL�SCHT WERDEN
%\section{FEM Check-Up}
%\label{sec:fem}
%
%When designing parts, there is always a trade-off between safety against failure and weight. However a more massive construction does not always imply more stability, which is why one needs to carefully evaluate the distribution of forces. For complex three dimensional parts, those forces cannot be computed directly, instead the \textquotedblleft finite element method\textquotedblright (FEM) is used. The CAD model is subdivided into small tetrahedrons, and for a specific load case, the forces acting on each tetrahedron are numerically computed, resulting in an accurate estimate of the internal forces and stresses for the part.\\
%This method was used to determine the rigidity under load of every part of \textsc{Scubo}. The thruster forces are too small to result in critical forces for the mounting, but the hull, which is designed to withstand an overpressure of three bars, must sustain substantial forces.\\
%In order to strengthen the top and bottom carbon faces, we insert eight aluminium supports between the outer carbon walls and the tube in the middle, four on top and four at the bottom. To minimize the additional weight while uniformly distributing the forces, the supports have a truss structure that was tested and optimized by means of the FEM method.\\
%The two aluminium hatches which close the carbon box bear the largest forces, because the pressure acts on a large surface that has no additional supports. Thus the inner face of each hatch has an elaborate structure of cross beams. It was designed performing many small adjustments to the structure and comparing the resulting stresses under an outer overpressure of three bars, calculated using the FEM method in \textsc{Siemens NX}.\\


%\section{Manufacturing<-raus}
%\label{sec:manufacturing}
%\textsc{Scubo} consists mainly of aluminium and carbon. All our aluminium parts are manufactured at our sponsors \textsc{Libs} and \textsc{Qualicut}. Responsible for the carbon box is our sponsor \textsc{Carbomill} in cooperation with \textsc{Swissfibertec}. The aluminium parts need to undergo an anodizing treatment to be resistant against salt water which is performed by \textsc{Alumex}.\\

%\section{CAD-System<-raus}
%\label{sec:cadsystem}
%Unabh\"angiges kapitel, welches CAD wir brauchten und vieleicht kurz das wir TOp Down etc gemacht haben. Max 1/3 seite.\\
%\section{Datasheet<-raus ABER: SPECS WIE SPEED SUPER! ABER BEI ANFANG HIN!}
%\label{sec:datasheet}
%All the following data, are either measurement from our CAD model  or from calculation we done in \textsc{Matlab} in conjunction with the system modelling. All the specifications are up to end of January 2016.
%\begin{table}[h]
%	\begin{center}
%		\begin{tabular}{|l|c|}
%			\hline
%			Length & 748 mm \\
%			Width & 348 mm \\
%			Height & 322 mm \\
%			\hline
%			Weight & 25 kg \\
%			\hline
%			Speed & 2.4 $m/s$, 4.67 kn  \\
%			Acceleration Ø & 1.5 $m/s^2$\\
%			\hline
%			Operational depth & 20 m \\	
%			\hline	
%		\end{tabular}
%		\caption{Facts of our robot \textsc{Scubo}}\vspace{1ex}
%		\label{tab:datasheet}
%	\end{center}
%\end{table}

